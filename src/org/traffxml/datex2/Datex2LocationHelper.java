package org.traffxml.datex2;
/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml-datex2 library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.datex2.Datex2Helper.D2Context;
import org.traffxml.datex2.Datex2Helper.SituationRecordContext;
import org.traffxml.lib.alertclocation.AlertC;
import org.traffxml.lib.alertclocation.AlertCLocation;
import org.traffxml.lib.alertclocation.AlertCLocation.LocationClass;
import org.traffxml.lib.alertclocation.AlertCPoint;
import org.traffxml.lib.alertclocation.Segment;
import org.traffxml.traff.LatLon;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffLocation.Directionality;
import org.traffxml.traff.TraffLocation.Fuzziness;
import org.traffxml.traff.TraffLocation.RoadClass;

import eu.datex2.schema._2._2_0.AlertCDirectionEnum;
import eu.datex2.schema._2._2_0.AlertCLinear;
import eu.datex2.schema._2._2_0.AlertCMethod2Linear;
import eu.datex2.schema._2._2_0.AlertCMethod2Point;
import eu.datex2.schema._2._2_0.AlertCMethod4Linear;
import eu.datex2.schema._2._2_0.AlertCMethod4Point;
import eu.datex2.schema._2._2_0.DirectionEnum;
import eu.datex2.schema._2._2_0.DistanceAlongLinearElement;
import eu.datex2.schema._2._2_0.DistanceFromLinearElementReferent;
import eu.datex2.schema._2._2_0.DistanceFromLinearElementStart;
import eu.datex2.schema._2._2_0.GroupOfLocations;
import eu.datex2.schema._2._2_0.Itinerary;
import eu.datex2.schema._2._2_0.ItineraryByIndexedLocations;
import eu.datex2.schema._2._2_0.Linear;
import eu.datex2.schema._2._2_0.LinearReferencingDirectionEnum;
import eu.datex2.schema._2._2_0.Location;
import eu.datex2.schema._2._2_0.LocationContainedInItinerary;
import eu.datex2.schema._2._2_0.MultilingualString;
import eu.datex2.schema._2._2_0.MultilingualStringValue;
import eu.datex2.schema._2._2_0.NetworkLocation;
import eu.datex2.schema._2._2_0.OpenlrBaseLocationReferencePoint;
import eu.datex2.schema._2._2_0.OpenlrExtendedLinear;
import eu.datex2.schema._2._2_0.Point;
import eu.datex2.schema._2._2_0.PointByCoordinates;
import eu.datex2.schema._2._2_0.ReferentTypeEnum;
import eu.datex2.schema._2._2_0.TpegFramedPoint;
import eu.datex2.schema._2._2_0.TpegJunction;
import eu.datex2.schema._2._2_0.TpegLinearLocation;
import eu.datex2.schema._2._2_0.TpegNonJunctionPoint;
import eu.datex2.schema._2._2_0.TpegPoint;
import eu.datex2.schema._2._2_0.TpegPointLocation;
import eu.datex2.schema._2._2_0.TpegSimplePoint;

/**
 * Helper class for location conversion.
 * 
 * <p>Members of this class are meant to be called only from {@link Datex2Helper} or internally. For this
 * reason, this class has no public members.
 */
public class Datex2LocationHelper {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	/** CID (unique integer identifier) mappings by ISO country code */
	static Map<String, Integer> ISO_TO_CID = new HashMap<String, Integer>();
	/** ISO country code mappings by CID (unique integer identifier) */
	static HashMap<Integer, String> CID_TO_ISO = new HashMap<Integer, String>();

	static {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(Datex2LocationHelper.class.getResourceAsStream("countries.csv")));
			// first line contains column headers, skip
			br.readLine();
			String line;
			while ((line = br.readLine()) != null) {
				String[] fields = Datex2Helper.COLON_PATTERN.split(line);
				/*
				 * Fields are: cid, ccd, iso, ecc, tabcd_min, tabcd_max, cname
				 * Of these, only the first three are currently evaluated.
				 */
				if (fields.length < 3)
					continue;
				if (!fields[2].matches("[a-zA-Z]{2}"))
					continue;
				if (fields[0].matches("[0-9]+")) {
					CID_TO_ISO.put(Integer.valueOf(fields[0]), fields[2].toUpperCase());
					ISO_TO_CID.put(fields[2].toUpperCase(), Integer.valueOf(fields[0]));
				}
			}
		} catch (IOException e) {
			LOG.debug("{}", e);
		}
	}

	/**
	 * Converts a Datex 2 GroupOfLocations to a TraFF location.
	 * 
	 * @param context The SituationRecordContext for the enclosing SituationRecord
	 * @param groupOfLocations The GroupOfLocations to convert
	 * 
	 * @return The TraFF location, or null if the SituationRecord contained no usable location data
	 */
	static TraffLocation getTraffLocationFromGroupOfLocations(SituationRecordContext context, GroupOfLocations groupOfLocations) {
		if (groupOfLocations instanceof Location)
			return Datex2LocationHelper.getTraffLocationFromLocation(context, (Location) groupOfLocations);
		else if (groupOfLocations instanceof Itinerary)
			return Datex2LocationHelper.getTraffLocationFromItinerary(context, (Itinerary) groupOfLocations);
		else {
			LOG.warn("Unsupported GroupOfLocations subclass: " + groupOfLocations.getClass().getCanonicalName());
			return null;
		}
	}

	private static Float getDistanceFromDistanceAlongLinearElement(DistanceAlongLinearElement distanceAlongLinearElement) {
		if (distanceAlongLinearElement == null)
			return null;
		if (distanceAlongLinearElement instanceof DistanceFromLinearElementReferent) {
			DistanceFromLinearElementReferent dfler = (DistanceFromLinearElementReferent) distanceAlongLinearElement;
			if (dfler.getFromReferent().getReferentType() == ReferentTypeEnum.REFERENCE_MARKER)
				return dfler.getDistanceAlong() / 1000.0f;
			// TODO other referent types?
		} else if (distanceAlongLinearElement instanceof DistanceFromLinearElementStart) {
			// TODO does this always hold true?
			return ((DistanceFromLinearElementStart) distanceAlongLinearElement).getDistanceAlong() / 1000.0f;
		}
		return null;
	}

	/**
	 * Expands an AlertC location, identified by its country, location table and location code tuple.
	 * 
	 * <p>AlertC allows for multiple locations to specify the country:
	 * 
	 * <p>The location tables use a unique numeric identifier, which is, however, not used directly and has not
	 * been seen in a Datex 2 context.
	 * 
	 * <p>RDS-TMC, the original application for AlertC, uses the CCD, a single hex digit. The combination of CCD
	 * and TABCD was originally designed to be unique by assigning different TABCD ranges to countries
	 * sharing the same CCD, but this has since been abandoned and some collisions do occur, though all
	 * observed cases are between countries located on different continents. Another code, the ECC, can be
	 * used along with the CCD to uniquely identify the country, but this is not done in Datex 2.
	 * 
	 * <p>Some Datex 2 sources use the ISO country code, which is unique and readily translates into a CID.
	 * 
	 * @param country The CCD or ISO code for the country
	 * @param tabcd The location table identifier
	 * @param lcid The location code (must be a code that can be looked up in the LCL, not a special/INTER-ROAD location code)
	 * 
	 * @return The expanded location, or null if no matching location was found
	 */
	private static AlertCLocation getExpandedAlertCLocation(String country, String tabcd, int lcid) {
		int tabcdInt = -1;
		try {
			tabcdInt = Integer.valueOf(tabcd);
		} catch (NumberFormatException e) {
			/*
			System.err.println("TABCD is not a valid integer: " + tabcd);
			e.printStackTrace();
			 */
		}
		/*
		 * TODO Attempt to fix bogus tabcd values, real-world examples:
		 * Some Belgian sources report 601 instead of 1 (6 being both the CCD and CID for Belgium).
		 * Some German sources report 15 (the version number, outside the assigned TABCD range) instead of 1
		 * Some Dutch sources report 5.17 instead of 17 (the assigned range being 17–24)
		 */
		/* No valid TABCD found, location cannot be expanded */
		if (tabcdInt == -1)
			return null;

		if (country.matches("[0-9a-fA-F]")) {
			/*
			 * ccd is a valid CCD (single hex digit).
			 * Within Europe, CCD/TABCD uniquely identifies the country. Globally, collisions are possible:
			 * For example, 3/1 is used by both Andorra and Victoria (Australia). A/1 is used by both Austria
			 * and Singapore. The US and Canada use CCDs from 1 to E with TABCDs from 1 to 36, which collides
			 * with numerous other countries.
			 * Such collisions are not an issue as long as both the input we process and the location table
			 * are constrained to an area that is free from collisions. Otherwise we need to use the ISO code
			 * or CID, both of which are unique.
			 */
			return AlertC.getLocation(country, tabcdInt, lcid);
		} else if (country.matches("[a-zA-Z]{2}")) {
			/* ccd is the ISO code, not the CCD */
			Integer cid = ISO_TO_CID.get(country.toUpperCase());
			if (cid == null)
				/* unknown CID */
				return null;
			else
				/* resolve location with CID */
				return AlertC.getLocation(cid, tabcdInt, lcid);
		} else
			/* ccd is not in a supported format */
			return null;
	}

	/**
	 * Returns the road class for the location of the message.
	 * 
	 * <p>AlertC has no dedicated type for trunk roads: Some countries may classify motorway-grade trunk roads as motorways
	 * for AlertC purposes. Other countries may classify them as order 1 roads ({@code RoadClass#PRIMARY}). In this case,
	 * the next-lower road level may either be classified as the same level, or as {@code RoadClass#SECONDARY}.
	 * 
	 * <p>For this reason, {@code RoadClass#TRUNK} will never be returned for some countries.
	 * 
	 * <p>Also, AlertC knows only two types of ring roads: motorway and non-motorway. For ring roads of the latter type,
	 * {@code RoadClass#OTHER} will be returned.
	 * 
	 * @param message
	 * 
	 * @return The road class, or null if the location does not refer to a road
	 */
	private static RoadClass getRoadClass(AlertCLocation location) {
		boolean hasTrunkRoads = hasTrunkRoads(location.cid, location.tabcd);
		while (location != null) {
			if (LocationClass.LINE.equals(location.category)) {
				if (location.tcd == 1) {
					switch (location.stcd) {
					case 1:
						return RoadClass.MOTORWAY;
					case 2:
						return hasTrunkRoads ? RoadClass.TRUNK : RoadClass.PRIMARY;
					case 3:
						return hasTrunkRoads ? RoadClass.PRIMARY : RoadClass.SECONDARY;
					case 4:
						return hasTrunkRoads ? RoadClass.SECONDARY: RoadClass.TERTIARY;
					default:
						return RoadClass.OTHER;
					}
				} else if ((location.tcd == 2) && (location.stcd == 1)) {
					return RoadClass.MOTORWAY;
				}
			}

			if (location instanceof AlertCPoint) {
				if (((AlertCPoint) location).road != null)
					location = ((AlertCPoint) location).road;
				else
					location = ((AlertCPoint) location).segment;
			} else if (location instanceof Segment) {
				if (((Segment) location).road != null)
					location = ((Segment) location).road;
				else
					location = ((Segment) location).segment;
			} else
				return null;
		}

		return null;
	}

	/**
	 * Returns a string from a MultilingualString.
	 * 
	 * <p>This returns the first non-empty string, regardless of language.
	 * 
	 * @param multilingualString
	 * @return the first non-empty string, or null (including if {@code nultilingualString} contains no strings)
	 */
	private static String getStringFromMultilingualString(MultilingualString multilingualString) {
		if ((multilingualString == null) || (multilingualString.getValues() == null)
				|| (multilingualString.getValues().getValue() == null))
			return null;
		for (MultilingualStringValue value : multilingualString.getValues().getValue())
			if (!value.getValue().isEmpty())
				return value.getValue();
		return null;
	}

	private static TraffLocation getTraffLocationFromAlertCLinear(Datex2LocationHelper.GroupOfLocationsContext context, AlertCLinear alertCLinear) {
		AlertCLocation primary;
		AlertCLocation secondary;
		AlertCLocation enclosing = null;
		AlertCDirectionEnum direction = null;
		D2Context d2c = context.parentContext.parentContext.parentContext;
		// FIXME catch NPEs (though we should never be getting any for compliant and unmodified XML input)
		if (alertCLinear instanceof AlertCMethod2Linear) {
			AlertCMethod2Linear acm2l = (AlertCMethod2Linear) alertCLinear;
			primary = Datex2LocationHelper.getExpandedAlertCLocation(
					(d2c.cc == null) ? alertCLinear.getAlertCLocationCountryCode() : d2c.cc,
					(d2c.ltn == null) ? alertCLinear.getAlertCLocationTableNumber() : d2c.ltn,
					acm2l.getAlertCMethod2PrimaryPointLocation().getAlertCLocation().getSpecificLocation().intValue());
			secondary = Datex2LocationHelper.getExpandedAlertCLocation(
					(d2c.cc == null) ? alertCLinear.getAlertCLocationCountryCode() : d2c.cc,
					(d2c.ltn == null) ? alertCLinear.getAlertCLocationTableNumber() : d2c.ltn,
					acm2l.getAlertCMethod2SecondaryPointLocation().getAlertCLocation().getSpecificLocation().intValue());
			if (acm2l.getAlertCDirection() != null)
				direction = acm2l.getAlertCDirection().getAlertCDirectionCoded();
		} else if (alertCLinear instanceof AlertCMethod4Linear) {
			AlertCMethod4Linear acm4l = (AlertCMethod4Linear) alertCLinear;
			primary = Datex2LocationHelper.getExpandedAlertCLocation(
					(d2c.cc == null) ? alertCLinear.getAlertCLocationCountryCode() : d2c.cc,
					(d2c.ltn == null) ? alertCLinear.getAlertCLocationTableNumber() : d2c.ltn,
					acm4l.getAlertCMethod4PrimaryPointLocation().getAlertCLocation().getSpecificLocation().intValue());
			secondary = Datex2LocationHelper.getExpandedAlertCLocation(
					(d2c.cc == null) ? alertCLinear.getAlertCLocationCountryCode() : d2c.cc,
					(d2c.ltn == null) ? alertCLinear.getAlertCLocationTableNumber() : d2c.ltn,
					acm4l.getAlertCMethod4SecondaryPointLocation().getAlertCLocation().getSpecificLocation().intValue());
			if (acm4l.getAlertCDirection() != null)
				direction = acm4l.getAlertCDirection().getAlertCDirectionCoded();
		} else
			throw new IllegalArgumentException("Unsupported AlertCLinear subclass: " + alertCLinear.getClass().getSimpleName());
		if ((primary == null) || (secondary == null))
			return null;
		TraffLocation.Builder builder = new TraffLocation.Builder();

		builder.setFuzziness(Fuzziness.LOW_RES);
		if (secondary instanceof AlertCPoint)
			builder.setFrom(Datex2LocationHelper.getTraffPointFromAlertCPoint((AlertCPoint) secondary));
		if (primary instanceof AlertCPoint)
			builder.setTo(Datex2LocationHelper.getTraffPointFromAlertCPoint((AlertCPoint) primary));

		if (direction == AlertCDirectionEnum.BOTH)
			builder.setDirectionality(Directionality.BOTH_DIRECTIONS);
		else if ((direction == AlertCDirectionEnum.NEGATIVE) || (direction == AlertCDirectionEnum.POSITIVE))
			builder.setDirectionality(Directionality.ONE_DIRECTION);

		// TODO builder.setTerritory
		// TODO builder.setTown
		// TODO figure out if we are on a ring road and insert an auxiliary point if we are

		/* get enclosing linear location */
		if (!primary.equals(secondary))
			enclosing = primary.getEnclosingLocation(secondary);
		else if (((AlertCPoint) primary).segment != null)
			enclosing = ((AlertCPoint) primary).segment;
		else if (((AlertCPoint) primary).road != null)
			enclosing = ((AlertCPoint) primary).road;

		if (enclosing != null) {
			builder.setRoadClass(Datex2LocationHelper.getRoadClass(enclosing));
			builder.setRoadRef(enclosing.getRoadNumber());
			if (enclosing.roadName != null)
				builder.setRoadName(enclosing.roadName.name);
			if (CID_TO_ISO.containsKey(enclosing.cid))
				builder.setCountry(CID_TO_ISO.get(enclosing.cid));
			if (direction == AlertCDirectionEnum.NEGATIVE) {
				if (enclosing.name2 != null)
					builder.setOrigin(enclosing.name2.name);
				if (enclosing.name1 != null)
					builder.setDestination(enclosing.name1.name);
			} else {
				if (enclosing.name1 != null)
					builder.setOrigin(enclosing.name1.name);
				if (enclosing.name2 != null)
					builder.setDestination(enclosing.name2.name);
			}
		}
		return builder.build();
	}

	private static TraffLocation getTraffLocationFromAlertCPoint(Datex2LocationHelper.GroupOfLocationsContext context, eu.datex2.schema._2._2_0.AlertCPoint alertCPoint) {
		AlertCLocation at;
		AlertCLocation from = null;
		AlertCLocation to = null;
		AlertCDirectionEnum direction = null;
		D2Context d2c = context.parentContext.parentContext.parentContext;
		// FIXME catch NPEs (though we should never be getting any for compliant and unmodified XML input)
		if (alertCPoint instanceof AlertCMethod2Point) {
			AlertCMethod2Point acm2p = (AlertCMethod2Point) alertCPoint;
			at = Datex2LocationHelper.getExpandedAlertCLocation(
					(d2c.cc == null) ? alertCPoint.getAlertCLocationCountryCode() : d2c.cc,
					(d2c.ltn == null) ? alertCPoint.getAlertCLocationTableNumber() : d2c.ltn,
					acm2p.getAlertCMethod2PrimaryPointLocation().getAlertCLocation().getSpecificLocation().intValue());
			if (acm2p.getAlertCDirection() != null)
				direction = acm2p.getAlertCDirection().getAlertCDirectionCoded();
		} else if (alertCPoint instanceof AlertCMethod4Point) {
			AlertCMethod4Point acm4p = (AlertCMethod4Point) alertCPoint;
			at = Datex2LocationHelper.getExpandedAlertCLocation(
					(d2c.cc == null) ? alertCPoint.getAlertCLocationCountryCode() : d2c.cc,
					(d2c.ltn == null) ? alertCPoint.getAlertCLocationTableNumber() : d2c.ltn,
					acm4p.getAlertCMethod4PrimaryPointLocation().getAlertCLocation().getSpecificLocation().intValue());
			if (acm4p.getAlertCDirection() != null)
				direction = acm4p.getAlertCDirection().getAlertCDirectionCoded();
		} else
			throw new IllegalArgumentException("Unsupported AlertCPoint subclass: " + alertCPoint.getClass().getSimpleName());
		if ((at == null) || !(at instanceof AlertCPoint))
			return null;
		TraffLocation.Builder builder = new TraffLocation.Builder();

		builder.setFuzziness(Fuzziness.LOW_RES);
		builder.setAt(Datex2LocationHelper.getTraffPointFromAlertCPoint((AlertCPoint) at));

		if (CID_TO_ISO.containsKey(at.cid))
			builder.setCountry(CID_TO_ISO.get(at.cid));
		if (direction == AlertCDirectionEnum.BOTH)
			builder.setDirectionality(Directionality.BOTH_DIRECTIONS);
		else if ((direction == AlertCDirectionEnum.NEGATIVE) || (direction == AlertCDirectionEnum.POSITIVE))
			builder.setDirectionality(Directionality.ONE_DIRECTION);

		AlertCLocation enclosing = ((AlertCPoint) at).segment != null ? ((AlertCPoint) at).segment : ((AlertCPoint) at).road;
		if (direction == AlertCDirectionEnum.NEGATIVE) {
			from = ((AlertCPoint) at).getPosOffset();
			to = ((AlertCPoint) at).getNegOffset();
		} else {
			from = ((AlertCPoint) at).getNegOffset();
			to = ((AlertCPoint) at).getPosOffset();
		}
		// TODO figure out if we are on a ring road (if not, drop one of these two points)
		if (from != null) {
			builder.setFrom(Datex2LocationHelper.getTraffPointFromAlertCPoint((AlertCPoint) from));
			enclosing = enclosing.getEnclosingLocation(from);
		}
		if (to != null) {
			builder.setTo(Datex2LocationHelper.getTraffPointFromAlertCPoint((AlertCPoint) to));
			enclosing = enclosing.getEnclosingLocation(to);
		}
		if (direction == AlertCDirectionEnum.NEGATIVE) {
			if (enclosing.name2 != null)
				builder.setOrigin(enclosing.name2.name);
			if (enclosing.name1 != null)
				builder.setDestination(enclosing.name1.name);
		} else {
			if (enclosing.name1 != null)
				builder.setOrigin(enclosing.name1.name);
			if (enclosing.name2 != null)
				builder.setDestination(enclosing.name2.name);
		}

		// TODO builder.setTerritory
		// TODO builder.setTown

		builder.setRoadClass(Datex2LocationHelper.getRoadClass(at));
		builder.setRoadRef(at.getRoadNumber());
		if (at.roadName != null)
			builder.setRoadName(at.roadName.name);
		return builder.build();
	}

	private static TraffLocation getTraffLocationFromItinerary(SituationRecordContext context, Itinerary itinerary) {
		// nothing usable here (locationForDisplay is not necessarily suitable for location matching)
		if (itinerary instanceof ItineraryByIndexedLocations)
			return Datex2LocationHelper.getTraffLocationFromItineraryByIndexedLocations(context, (ItineraryByIndexedLocations) itinerary);
		else {
			LOG.warn("Unsupported Itinerary subclass: " + itinerary.getClass().getCanonicalName());
			return null;
		}
	}

	private static TraffLocation getTraffLocationFromItineraryByIndexedLocations(SituationRecordContext context,
			ItineraryByIndexedLocations itineraryByIndexedLocations) {
		List<TraffLocation> traffLocations = new ArrayList<TraffLocation>(
				itineraryByIndexedLocations.getLocationContainedInItinerary().size());
		Set<RoadClass> rclasses = EnumSet.noneOf(RoadClass.class);
		for (LocationContainedInItinerary lcii : itineraryByIndexedLocations.getLocationContainedInItinerary()) {
			TraffLocation location = getTraffLocationFromLocation(context, lcii.getLocation());
			if (location != null) {
				traffLocations.add(location);
				if (location.roadClass != null)
					rclasses.add(location.roadClass);
			}
		}

		// Special handling for zero or one usable locations
		if (traffLocations.size() == 0) {
			LOG.warn("No usable locations in ItineraryByIndexedLocations for " + context.situationRecord.getId());
			return null;
		} else if (traffLocations.size() == 1)
			return traffLocations.get(0);

		// General case
		TraffLocation.Builder builder = new TraffLocation.Builder();
		TraffLocation first = traffLocations.get(0);
		// TODO via
		TraffLocation last = traffLocations.get(traffLocations.size() - 1);
		if ((first.country != null) && (last.country != null) && first.country.equals(last.country))
			builder.setCountry(first.country);
		builder.setDestination(last.destination);
		/*
		 * TODO ItineraryByIndexedLocations has no explicit directionality. Where constituent
		 * locations support it, ONE_DIRECTION seems to be predominant. Otherwise libtraff will
		 * return the default of BOTH_DIRECTIONS, which is undesirable here.
		 */
		builder.setDirectionality(Directionality.ONE_DIRECTION);
		if (first.fuzziness == last.fuzziness)
			builder.setFuzziness(first.fuzziness);
		builder.setOrigin(first.origin);
		if (rclasses.size() == 1)
			for (RoadClass rclass : rclasses)
				builder.setRoadClass(rclass);
		// TODO roadIsUrban
		if ((first.roadName != null) && (last.roadName != null) && first.roadName.equals(last.roadName))
			builder.setRoadName(first.roadName);
		if ((first.roadRef != null) && (last.roadRef != null) && first.roadRef.equals(last.roadRef))
			builder.setRoadRef(first.roadRef);
		if ((first.territory != null) && (last.territory != null) && first.territory.equals(last.territory))
			builder.setTerritory(first.territory);
		if ((first.town != null) && (last.town != null) && first.town.equals(last.town))
			builder.setTown(first.town);
		if (first.at != null)
			builder.setFrom(first.at);
		else
			builder.setFrom(first.from);
		if (last.at != null)
			builder.setTo(last.at);
		else
			builder.setTo(last.to);
		return builder.build();
	}

	private static TraffLocation getTraffLocationFromLinear(SituationRecordContext context, Linear linear) {
		Datex2LocationHelper.GroupOfLocationsContext childContext = new Datex2LocationHelper.GroupOfLocationsContext(context, linear);
		List<TraffLocation> locations = new ArrayList<TraffLocation>();
		Directionality directionality = null;
		String country = null;
		String roadRef = null;
		String roadName = null;
		String fromName = null;
		String toName = null;
		Float fromDistance = null;
		Float toDistance = null;
		if (linear.getTpegLinearLocation() != null) {
			locations.add(Datex2LocationHelper.getTraffLocationFromTpegLinearLocation(childContext, linear.getTpegLinearLocation()));
		}
		if (linear.getAlertCLinear() != null) {
			locations.add(Datex2LocationHelper.getTraffLocationFromAlertCLinear(childContext, linear.getAlertCLinear()));
			/* get directionality and junction names, in case we cannot determine them otherwise */
			// TODO determine country
			if (linear.getAlertCLinear() instanceof AlertCMethod2Linear) {
				AlertCMethod2Linear acm2l = (AlertCMethod2Linear) linear.getAlertCLinear();
				if ((directionality == null) && (acm2l.getAlertCDirection() != null)) {
					AlertCDirectionEnum direction = acm2l.getAlertCDirection().getAlertCDirectionCoded();
					if (direction == AlertCDirectionEnum.BOTH)
						directionality = Directionality.BOTH_DIRECTIONS;
					else if ((direction == AlertCDirectionEnum.NEGATIVE) || (direction == AlertCDirectionEnum.POSITIVE))
						directionality = Directionality.ONE_DIRECTION;
				}
				if (fromName == null)
					fromName = Datex2LocationHelper.getStringFromMultilingualString(acm2l.getAlertCMethod2SecondaryPointLocation().getAlertCLocation().getAlertCLocationName());
				if (toName == null)
					toName = Datex2LocationHelper.getStringFromMultilingualString(acm2l.getAlertCMethod2PrimaryPointLocation().getAlertCLocation().getAlertCLocationName());
			} else if (linear.getAlertCLinear() instanceof AlertCMethod4Linear) {
				AlertCMethod4Linear acm4l = (AlertCMethod4Linear) linear.getAlertCLinear();
				if ((directionality == null) && (acm4l.getAlertCDirection() != null)) {
					AlertCDirectionEnum direction = acm4l.getAlertCDirection().getAlertCDirectionCoded();
					if (direction == AlertCDirectionEnum.BOTH)
						directionality = Directionality.BOTH_DIRECTIONS;
					else if ((direction == AlertCDirectionEnum.NEGATIVE) || (direction == AlertCDirectionEnum.POSITIVE))
						directionality = Directionality.ONE_DIRECTION;
				}
				if (fromName == null)
					fromName = Datex2LocationHelper.getStringFromMultilingualString(acm4l.getAlertCMethod4SecondaryPointLocation().getAlertCLocation().getAlertCLocationName());
				if (toName == null)
					toName = Datex2LocationHelper.getStringFromMultilingualString(acm4l.getAlertCMethod4PrimaryPointLocation().getAlertCLocation().getAlertCLocationName());
			}
			// TODO AlertCLinearByCode (directionality and location name, which is the segment name here)
		}
		if (linear.getLinearWithinLinearElement() != null) {
			/*
			 * TODO linearLocation
			 * If this is of type DistanceFromLinearElementReferent and fromReferent has coordinates, then:
			 * - if fromReferent is of type INTERSECTION, use it as the end point (including junction names)
			 * - if distance from fromReferent is less than 1000 m, use it as the end point (including the name)
			 */
			/* get directionality and auxiliary information, in case we cannot determine them otherwise */
			if (directionality == null) {
				if (linear.getLinearWithinLinearElement().getDirectionRelativeOnLinearSection() == LinearReferencingDirectionEnum.BOTH)
					directionality = Directionality.BOTH_DIRECTIONS;
				else if ((linear.getLinearWithinLinearElement().getDirectionRelativeOnLinearSection() == LinearReferencingDirectionEnum.ALIGNED)
						|| (linear.getLinearWithinLinearElement().getDirectionRelativeOnLinearSection() == LinearReferencingDirectionEnum.OPPOSITE))
					directionality = Directionality.ONE_DIRECTION;
			}
			if ((roadRef == null) && (linear.getLinearWithinLinearElement().getLinearElement() != null))
				roadRef = linear.getLinearWithinLinearElement().getLinearElement().getRoadNumber();
			if ((roadName == null) && (linear.getLinearWithinLinearElement().getLinearElement() != null))
				roadName = Datex2LocationHelper.getStringFromMultilingualString(linear.getLinearWithinLinearElement().getLinearElement().getRoadName());
			if (fromDistance == null)
				fromDistance = Datex2LocationHelper.getDistanceFromDistanceAlongLinearElement(linear.getLinearWithinLinearElement().getFromPoint());
			if (toDistance == null)
				toDistance = Datex2LocationHelper.getDistanceFromDistanceAlongLinearElement(linear.getLinearWithinLinearElement().getToPoint());
			/*
			 * TODO if we cannot resolve the location, harvest fields:
			 * directionBoundOnLinearSection
			 */
		}
		if ((linear.getLinearExtension() != null) && (linear.getLinearExtension().getOpenlrExtendedLinear() != null)) {
			TraffLocation location = Datex2LocationHelper.getTraffLocationFromOpenlrExtendedLinear(childContext, linear.getLinearExtension().getOpenlrExtendedLinear());
			if (location != null) {
				locations.add(location);
				/* get directionality and auxiliary information, in case we cannot determine them otherwise */
				if (directionality == null) {
					if (linear.getLinearExtension().getOpenlrExtendedLinear().getOppositeDirection() == null)
						directionality = Directionality.ONE_DIRECTION;
					else
						directionality = Directionality.BOTH_DIRECTIONS;
				}
			}
		}
		while (locations.contains(null))
			locations.remove(null);
		for (TraffLocation location : locations)
			if (location.country != null) {
				country = location.country;
				break;
			}
		if ((country == null) && (context.parentContext.parentContext.country != null) && context.parentContext.parentContext.country.matches("[a-zA-Z]{2}"))
			country = context.parentContext.parentContext.country.toUpperCase();
		TraffLocation res = Datex2LocationHelper.mergeTraffLocations(locations);
		if (res == null) {
			LOG.warn("No supported location information found in " + context.situationRecord.getId());
			return null;
		}
		TraffLocation.Builder builder = new TraffLocation.Builder(res);
		if (directionality != null)
			builder.setDirectionality(directionality);
		if (country != null)
			builder.setCountry(country);
		if (roadRef != null)
			builder.setRoadRef(roadRef);
		if (roadName != null)
			builder.setRoadName(roadName);
		if ((fromName != null) || (fromDistance != null)) {
			TraffLocation.Point.Builder pointBuilder = new TraffLocation.Point.Builder(res.from);
			if (fromName != null)
				pointBuilder.setJunctionName(fromName);
			if (fromDistance != null)
				pointBuilder.setDistance(fromDistance);
			builder.setFrom(pointBuilder.build());
		}
		if ((toName != null) || (toDistance != null)) {
			TraffLocation.Point.Builder pointBuilder = new TraffLocation.Point.Builder(res.to);
			if (toName != null)
				pointBuilder.setJunctionName(toName);
			if (toDistance != null)
				pointBuilder.setDistance(toDistance);
			builder.setTo(pointBuilder.build());
		}
		return builder.build();
	}

	private static TraffLocation getTraffLocationFromLocation(SituationRecordContext context, Location location) {
		// nothing usable here (locationForDisplay is not necessarily suitable for location matching)
		if (location instanceof NetworkLocation)
			return Datex2LocationHelper.getTraffLocationFromNetworkLocation(context, (NetworkLocation) location);
		else {
			LOG.warn("Unsupported Location subclass: " + location.getClass().getCanonicalName());
			return null;
		}
	}

	private static TraffLocation getTraffLocationFromNetworkLocation(SituationRecordContext context, NetworkLocation networkLocation) {
		TraffLocation location = null;
		if (networkLocation instanceof Linear)
			location = Datex2LocationHelper.getTraffLocationFromLinear(context, (Linear) networkLocation);
		else if (networkLocation instanceof Point)
			location = Datex2LocationHelper.getTraffLocationFromPoint(context, (Point) networkLocation);
		else
			LOG.warn("Unsupported NetworkLocation subclass: " + networkLocation.getClass().getCanonicalName());
		if (location == null)
			return null;
		TraffLocation.Builder builder = new TraffLocation.Builder(location);
		// TODO SupplementaryPositionalDescription
		// TODO Destination
		return builder.build();
	}

	private static TraffLocation getTraffLocationFromOpenlrExtendedLinear(Datex2LocationHelper.GroupOfLocationsContext childContext,
			OpenlrExtendedLinear openlrExtendedLinear) {
		TraffLocation.Builder builder = new TraffLocation.Builder();
		if (openlrExtendedLinear.getOppositeDirection() == null)
			builder.setDirectionality(Directionality.ONE_DIRECTION);
		else
			builder.setDirectionality(Directionality.BOTH_DIRECTIONS);
		/*
		 * TODO functional road classes
		 * OpenLR has eight classes, FRC_0 (highest) to FRC_7 (lowest), but mapping to actual road classes
		 * varies dramatically between users: for example, secondary maps to FRC_2 in Poland and FRC_4 in
		 * Sweden. Worse yet, FRC_0 in Poland can be either motorway or trunk.
		 */
		if (openlrExtendedLinear.getFirstDirection() != null) {
			if ((openlrExtendedLinear.getFirstDirection().getOpenlrLocationReferencePoint() != null)
					&& !openlrExtendedLinear.getFirstDirection().getOpenlrLocationReferencePoint().isEmpty())
				builder.setFrom(Datex2LocationHelper.getTraffPointFromOpenlrBaseLocationReferencePoint(openlrExtendedLinear.getFirstDirection().getOpenlrLocationReferencePoint().get(0)));
			if (openlrExtendedLinear.getFirstDirection().getOpenlrLastLocationReferencePoint() != null)
				builder.setTo(Datex2LocationHelper.getTraffPointFromOpenlrBaseLocationReferencePoint(openlrExtendedLinear.getFirstDirection().getOpenlrLastLocationReferencePoint()));
			return builder.build();
		} else
			return null;
	}

	private static TraffLocation getTraffLocationFromPoint(SituationRecordContext context, Point point) {
		Datex2LocationHelper.GroupOfLocationsContext childContext = new Datex2LocationHelper.GroupOfLocationsContext(context, point);
		List<TraffLocation> locations = new ArrayList<TraffLocation>();
		Directionality directionality = null;
		String country = null;
		String roadRef = null;
		String roadName = null;
		String atName = null;
		Float atDistance = null;
		if (point.getTpegPointLocation() != null) {
			locations.add(getTraffLocationFromTpegPointLocation(childContext, point.getTpegPointLocation()));
		}
		if (point.getAlertCPoint() != null) {
			locations.add(getTraffLocationFromAlertCPoint(childContext, point.getAlertCPoint()));
			/* get directionality and junction names, in case we cannot determine them otherwise */
			// TODO determine country
			if (point.getAlertCPoint() instanceof AlertCMethod2Point) {
				AlertCMethod2Point acm2p = (AlertCMethod2Point) point.getAlertCPoint();
				if ((directionality == null) && (acm2p.getAlertCDirection() != null)) {
					AlertCDirectionEnum direction = acm2p.getAlertCDirection().getAlertCDirectionCoded();
					if (direction == AlertCDirectionEnum.BOTH)
						directionality = Directionality.BOTH_DIRECTIONS;
					else if ((direction == AlertCDirectionEnum.NEGATIVE) || (direction == AlertCDirectionEnum.POSITIVE))
						directionality = Directionality.ONE_DIRECTION;
				}
				if (atName == null)
					atName = Datex2LocationHelper.getStringFromMultilingualString(acm2p.getAlertCMethod2PrimaryPointLocation().getAlertCLocation().getAlertCLocationName());
			} else if (point.getAlertCPoint() instanceof AlertCMethod4Point) {
				AlertCMethod4Point acm4p = (AlertCMethod4Point) point.getAlertCPoint();
				if ((directionality == null) && (acm4p.getAlertCDirection() != null)) {
					AlertCDirectionEnum direction = acm4p.getAlertCDirection().getAlertCDirectionCoded();
					if (direction == AlertCDirectionEnum.BOTH)
						directionality = Directionality.BOTH_DIRECTIONS;
					else if ((direction == AlertCDirectionEnum.NEGATIVE) || (direction == AlertCDirectionEnum.POSITIVE))
						directionality = Directionality.ONE_DIRECTION;
				}
				if (atName == null)
					atName = Datex2LocationHelper.getStringFromMultilingualString(acm4p.getAlertCMethod4PrimaryPointLocation().getAlertCLocation().getAlertCLocationName());
			}
		}
		if (point.getPointAlongLinearElement() != null) {
			/*
			 * TODO linearLocation
			 * If this is of type DistanceFromLinearElementReferent and fromReferent has coordinates, then:
			 * - if fromReferent is of type INTERSECTION, use it as the point (including junction names)
			 * - if distance from fromReferent is less than 1000 m, use it as the point (including the name)
			 */
			/* get directionality and auxiliary information, in case we cannot determine them otherwise */
			if (directionality == null) {
				if (point.getPointAlongLinearElement().getDirectionRelativeAtPoint() == LinearReferencingDirectionEnum.BOTH)
					directionality = Directionality.BOTH_DIRECTIONS;
				else if ((point.getPointAlongLinearElement().getDirectionRelativeAtPoint() == LinearReferencingDirectionEnum.ALIGNED)
						|| (point.getPointAlongLinearElement().getDirectionRelativeAtPoint() == LinearReferencingDirectionEnum.OPPOSITE))
					directionality = Directionality.ONE_DIRECTION;
			}
			if ((roadRef == null) && (point.getPointAlongLinearElement().getLinearElement() != null))
				roadRef = point.getPointAlongLinearElement().getLinearElement().getRoadNumber();
			if ((roadName == null) && (point.getPointAlongLinearElement().getLinearElement() != null))
				roadName = Datex2LocationHelper.getStringFromMultilingualString(point.getPointAlongLinearElement().getLinearElement().getRoadName());
			if (atDistance == null)
				atDistance = Datex2LocationHelper.getDistanceFromDistanceAlongLinearElement(point.getPointAlongLinearElement().getDistanceAlongLinearElement());
			/*
			 * TODO if we cannot resolve the location, harvest fields:
			 * directionBoundOnLinearSection
			 */
		}
		if (point.getPointByCoordinates() != null) {
			locations.add(getTraffLocationFromPointByCoordinates(childContext, point.getPointByCoordinates()));
			// TODO coordLocation
		}
		while (locations.contains(null))
			locations.remove(null);
		for (TraffLocation location : locations)
			if (location.country != null) {
				country = location.country;
				break;
			}
		if ((country == null) && (context.parentContext.parentContext.country != null) && context.parentContext.parentContext.country.matches("[a-zA-Z]{2}"))
			country = context.parentContext.parentContext.country.toUpperCase();
		TraffLocation res = Datex2LocationHelper.mergeTraffLocations(locations);
		if (res == null) {
			LOG.warn("No supported location information found in " + context.situationRecord.getId());
			return null;
		}
		TraffLocation.Builder builder = new TraffLocation.Builder(res);
		if (directionality != null)
			builder.setDirectionality(directionality);
		if (country != null)
			builder.setCountry(country);
		if (roadRef != null)
			builder.setRoadRef(roadRef);
		if (roadName != null)
			builder.setRoadName(roadName);
		if ((atName != null) || (atDistance != null)) {
			TraffLocation.Point.Builder pointBuilder = new TraffLocation.Point.Builder(res.at);
			if (atName != null)
				pointBuilder.setJunctionName(atName);
			if (atDistance != null)
				pointBuilder.setDistance(atDistance);
			builder.setAt(pointBuilder.build());
		}
		return builder.build();
	}

	private static TraffLocation getTraffLocationFromPointByCoordinates(Datex2LocationHelper.GroupOfLocationsContext context,
			PointByCoordinates pointByCoordinates) {
		TraffLocation.Builder builder = new TraffLocation.Builder();
		builder.setFuzziness(Fuzziness.NONE);
		// TODO bearing -> direction
		builder.setAt(Datex2LocationHelper.getTraffPointFromPointByCoordinates(pointByCoordinates));
		return builder.build();
	}

	private static TraffLocation getTraffLocationFromTpegLinearLocation(Datex2LocationHelper.GroupOfLocationsContext context, TpegLinearLocation tpegLinearLocation) {
		TraffLocation.Builder builder = new TraffLocation.Builder();
		builder.setFuzziness(Fuzziness.NONE);
		builder.setFrom(Datex2LocationHelper.getTraffPointFromTpegPoint(tpegLinearLocation.getFrom()));
		builder.setTo(Datex2LocationHelper.getTraffPointFromTpegPoint(tpegLinearLocation.getTo()));
		setBuilderDirectionalityFromTpegDirection(tpegLinearLocation.getTpegDirection(), builder);
		builder.setCountry(context.getCountry());
		// TODO origin, destination, roadName, roadRef
		return builder.build();
	}

	private static TraffLocation getTraffLocationFromTpegPointLocation(Datex2LocationHelper.GroupOfLocationsContext context, TpegPointLocation tpegPointLocation) {
		TraffLocation.Builder builder = new TraffLocation.Builder();
		builder.setFuzziness(Fuzziness.NONE);
		setBuilderDirectionalityFromTpegDirection(tpegPointLocation.getTpegDirection(), builder);
		if (tpegPointLocation instanceof TpegFramedPoint) {
			if (((TpegFramedPoint) tpegPointLocation).getFrom() != null)
				builder.setFrom(Datex2LocationHelper.getTraffPointFromTpegPoint(((TpegFramedPoint) tpegPointLocation).getFrom()));
			if (((TpegFramedPoint) tpegPointLocation).getTo() != null)
				builder.setTo(Datex2LocationHelper.getTraffPointFromTpegPoint(((TpegFramedPoint) tpegPointLocation).getTo()));
			builder.setAt(Datex2LocationHelper.getTraffPointFromTpegPoint(((TpegFramedPoint) tpegPointLocation).getFramedPoint()));
		} else if (tpegPointLocation instanceof TpegSimplePoint) {
			builder.setAt(Datex2LocationHelper.getTraffPointFromTpegPoint(((TpegSimplePoint) tpegPointLocation).getPoint()));
		} else
			throw new IllegalArgumentException("Unsupported TpegPointLocation subclass: " + tpegPointLocation.getClass().getCanonicalName());
		builder.setCountry(context.getCountry());
		// TODO origin, destination, roadName, roadRef
		return builder.build();
	}

	private static TraffLocation.Point getTraffPointFromAlertCPoint(AlertCPoint alertCPoint) {
		TraffLocation.Point.Builder builder = new TraffLocation.Point.Builder();
		if ((alertCPoint.name1 != null) && (alertCPoint.name1.name != null))
			builder.setJunctionName(alertCPoint.name1.name);
		if (alertCPoint.junctionNumber != null)
			builder.setJunctionRef(alertCPoint.junctionNumber);
		builder.setCoordinates(new LatLon(alertCPoint.yCoord, alertCPoint.xCoord));
		return builder.build();
	}

	private static TraffLocation.Point getTraffPointFromOpenlrBaseLocationReferencePoint(
			OpenlrBaseLocationReferencePoint openlrBaseLocationReferencePoint) {
		TraffLocation.Point.Builder builder = new TraffLocation.Point.Builder();
		if (openlrBaseLocationReferencePoint.getOpenlrCoordinate() != null)
			builder.setCoordinates(new LatLon(openlrBaseLocationReferencePoint.getOpenlrCoordinate().getLatitude(),
					openlrBaseLocationReferencePoint.getOpenlrCoordinate().getLongitude()));
		try {
			return builder.build();
		} catch (Exception e) {
			return null;
		}
	}

	private static TraffLocation.Point getTraffPointFromPointByCoordinates(
			PointByCoordinates pointByCoordinates) {
		TraffLocation.Point.Builder builder = new TraffLocation.Point.Builder();
		if (pointByCoordinates.getPointCoordinates() != null)
			builder.setCoordinates(new LatLon(pointByCoordinates.getPointCoordinates().getLatitude(),
					pointByCoordinates.getPointCoordinates().getLongitude()));
		try {
			return builder.build();
		} catch (Exception e) {
			return null;
		}
	}

	private static TraffLocation.Point getTraffPointFromTpegJunction(TpegJunction tpegJunction) {
		TraffLocation.Point.Builder builder = new TraffLocation.Point.Builder();
		if (tpegJunction.getPointCoordinates() != null)
			builder.setCoordinates(new LatLon(tpegJunction.getPointCoordinates().getLatitude(),
					tpegJunction.getPointCoordinates().getLongitude()));
		// TODO name and other attributes
		try {
			return builder.build();
		} catch (Exception e) {
			return null;
		}
	}

	private static TraffLocation.Point getTraffPointFromTpegNonJunctionPoint(TpegNonJunctionPoint tpegNonJunctionPoint) {
		TraffLocation.Point.Builder builder = new TraffLocation.Point.Builder();
		if (tpegNonJunctionPoint.getPointCoordinates() != null)
			builder.setCoordinates(new LatLon(tpegNonJunctionPoint.getPointCoordinates().getLatitude(),
					tpegNonJunctionPoint.getPointCoordinates().getLongitude()));
		// TODO name
		try {
			return builder.build();
		} catch (Exception e) {
			return null;
		}
	}

	private static TraffLocation.Point getTraffPointFromTpegPoint(TpegPoint tpegPoint) {
		if (tpegPoint instanceof TpegJunction)
			return getTraffPointFromTpegJunction((TpegJunction) tpegPoint);
		else if (tpegPoint instanceof TpegNonJunctionPoint)
			return getTraffPointFromTpegNonJunctionPoint((TpegNonJunctionPoint) tpegPoint);
		else
			throw new IllegalArgumentException("Unsupported TpegPoint subclass: " + tpegPoint.getClass().getCanonicalName());
	}

	/**
	 * Whether the location table has a category for trunk roads.
	 * 
	 * @param cid The unique country identifier
	 * @param tabcd The location table number
	 * 
	 * @return
	 */
	private static boolean hasTrunkRoads(int cid, int tabcd) {
		// TODO complete this table
		switch(cid << 8 | tabcd) {
		// TODO Czechia (ccd=2, tabcd=25)
		case 0x401: /* 4/1: Austria */
			return true;
			// case 0x601: /* 6/1: Belgium */
		case 0xC09: /* 12/9: Denmark */
			return false;
		case 0xF11: /* 15/17: Finland */
			return false;
		case 0x1020: /* 16/32: France */
			return false;
		case 0x1901: /* 25/1: Italy */
			return true;
			// case 0x2001: /* 32/1: Luxembourg */
			// case 0x2711: /* 39/17: Netherlands */
		case 0x2831: /* 40/49: Norway */
			return false;
			// case 0x2a33: /* 47/51: Slovakia */
		case 0x3111: /* 49/17: Spain */
			return true;
		case 0x3221: /* 50/33: Sweden */
			return false;
		case 0x3309: /* 51/9: Switzerland */
			return false;
		case 0x3A01: /* 58/1: Germany */
			return false;
		case 0x2be23: /* 702/35: Slovenia */
			return false;
		}
		return true;
	}

	/**
	 * Merges multiple representations for the same location to obtain the most detailed description.
	 * 
	 * <p>Locations are merged according to the following rules:
	 * <ul>
	 * <li>If a member is null in one location but non-null in another, the non-null value is used.</li>
	 * <li>The same rule applies to points as a whole, as well as members of each point.</li>
	 * <li>If an {@code at} point is specified, {@code via} and {@code not_via} are ignored.</li>
	 * <li>If a {@code via} point is specified, {@code not_via} is ignored.</li>
	 * <li>Where coordinates collide, the most accurate ones are preferred.</li>
	 * </ul>
	 * @param locations The list of locations (null entries will be ignored)
	 * @return The merged location, or null if {@code locations} was empty
	 */
	private static TraffLocation mergeTraffLocations(List<TraffLocation> locations) {
		while (locations.contains(null))
			locations.remove(null);
		if (locations.isEmpty())
			return null;
		if (locations.size() == 1)
			return locations.get(0);

		Map<String, Map<TraffLocation.Point, TraffLocation.Fuzziness>> points = new HashMap<String, Map<TraffLocation.Point, Fuzziness>>();
		points.put("from", new HashMap<TraffLocation.Point, TraffLocation.Fuzziness>());
		points.put("at", new HashMap<TraffLocation.Point, TraffLocation.Fuzziness>());
		points.put("via", new HashMap<TraffLocation.Point, TraffLocation.Fuzziness>());
		points.put("not_via", new HashMap<TraffLocation.Point, TraffLocation.Fuzziness>());
		points.put("to", new HashMap<TraffLocation.Point, TraffLocation.Fuzziness>());

		TraffLocation.Builder builder = new TraffLocation.Builder();
		String country = null;
		String destination = null;
		String direction = null;
		Directionality directionality = null;
		Fuzziness fuzziness = null;
		String origin = null;
		TraffLocation.Ramps ramps = null;
		RoadClass roadClass = null;
		Boolean roadIsUrban = null;
		String roadName = null;
		String roadRef = null;
		String territory = null;
		String town = null;

		for (TraffLocation location : locations) {
			if (country == null)
				country = location.country;
			if (destination == null)
				destination = location.destination;
			if (direction == null)
				direction = location.direction;
			if (directionality == null)
				directionality = location.directionality;
			if (origin == null)
				origin = location.origin;
			// TODO ramps
			if (roadClass == null)
				roadClass = location.roadClass;
			if (roadIsUrban == null)
				roadIsUrban = location.roadIsUrban;
			if (roadName == null)
				roadName = location.roadName;
			if (roadRef == null)
				roadRef = location.roadRef;
			if (territory == null)
				territory = location.territory;
			if (town == null)
				town = location.town;
			if (location.from != null)
				points.get("from").put(location.from, location.fuzziness);
			if (location.at != null)
				points.get("at").put(location.at, location.fuzziness);
			if (location.to != null)
				points.get("to").put(location.to, location.fuzziness);
			if (location.via != null)
				points.get("via").put(location.via, location.fuzziness);
			if (location.notVia != null)
				points.get("not_via").put(location.notVia, location.fuzziness);
		}
		/* Resolve collisions between at, via and not_via */
		if (!points.get("at").isEmpty()) {
			points.get("via").clear();
			points.get("not_via").clear();
		} else if (!points.get("via").isEmpty())
			points.get("not_via").clear();
		for (Map<TraffLocation.Point, Fuzziness> pointSet : points.values()) {
			if (pointSet.isEmpty())
				continue;
			fuzziness = null;
			LatLon coordinates = null;
			Float distance = null;
			String junctionName = null;
			String junctionRef = null;
			for (Map.Entry<TraffLocation.Point, Fuzziness> pointEntry : pointSet.entrySet()) {
				if ((fuzziness == null)
						|| ((fuzziness == Fuzziness.LOW_RES) && (pointEntry.getValue() != Fuzziness.LOW_RES))
						|| ((fuzziness != Fuzziness.NONE) && (pointEntry.getValue() == Fuzziness.NONE))) {
					fuzziness = pointEntry.getValue();
					coordinates = pointEntry.getKey().coordinates;
				}
				if (distance == null)
					distance = pointEntry.getKey().distance;
				if (junctionName == null)
					junctionName = pointEntry.getKey().junctionName;
				if (junctionRef == null)
					junctionRef = pointEntry.getKey().junctionRef;
			}
			TraffLocation.Point.Builder pointBuilder = new TraffLocation.Point.Builder();
			pointBuilder.setCoordinates(coordinates);
			pointBuilder.setDistance(distance);
			pointBuilder.setJunctionName(junctionName);
			pointBuilder.setJunctionRef(junctionRef);
			pointSet.clear();
			pointSet.put(pointBuilder.build(), fuzziness);
		}
		for (Map<TraffLocation.Point, Fuzziness> pointSet : points.values())
			for (Map.Entry<TraffLocation.Point, Fuzziness> pointEntry : pointSet.entrySet())
				if ((fuzziness == null)
						|| ((fuzziness == Fuzziness.LOW_RES) && (pointEntry.getValue() != Fuzziness.LOW_RES))
						|| ((fuzziness != Fuzziness.NONE) && (pointEntry.getValue() == Fuzziness.NONE)))
					fuzziness = pointEntry.getValue();

		builder.setFuzziness(fuzziness == Fuzziness.NONE ? null : fuzziness);
		for (TraffLocation.Point point : points.get("from").keySet())
			builder.setFrom(point);
		for (TraffLocation.Point point : points.get("at").keySet())
			builder.setAt(point);
		for (TraffLocation.Point point : points.get("to").keySet())
			builder.setTo(point);
		for (TraffLocation.Point point : points.get("via").keySet())
			builder.setVia(point);
		for (TraffLocation.Point point : points.get("not_via").keySet())
			builder.setNotVia(point);
		builder.setCountry(country);
		builder.setDestination(destination);
		builder.setDirection(direction);
		builder.setDirectionality(directionality);
		builder.setOrigin(origin);
		builder.setRamps(ramps);
		builder.setRoadClass(roadClass);
		builder.setRoadIsUrban(roadIsUrban);
		builder.setRoadName(roadName);
		builder.setRoadRef(roadRef);
		builder.setTerritory(territory);
		builder.setTown(town);
		return builder.build();
	}

	private static void setBuilderDirectionalityFromTpegDirection(DirectionEnum direction,
			TraffLocation.Builder builder) {
		if (direction == null)
			return;
		switch (direction) {
		case ALL_DIRECTIONS:
		case BOTH_WAYS:
			builder.setDirectionality(Directionality.BOTH_DIRECTIONS);
			break;
		case ANTICLOCKWISE:
		case CLOCKWISE:
		case INBOUND_TOWARDS_TOWN:
		case INNER_RING:
		case OPPOSITE:
		case OUTBOUND_FROM_TOWN:
		case OUTER_RING:
			// unidirectional but no match in TraFF
			builder.setDirectionality(Directionality.ONE_DIRECTION);
			break;
		case NORTH_BOUND:
			builder.setDirectionality(Directionality.ONE_DIRECTION);
			builder.setDirection("N");
			break;
		case NORTH_EAST_BOUND:
			builder.setDirectionality(Directionality.ONE_DIRECTION);
			builder.setDirection("NE");
			break;
		case EAST_BOUND:
			builder.setDirectionality(Directionality.ONE_DIRECTION);
			builder.setDirection("E");
			break;
		case SOUTH_EAST_BOUND:
			builder.setDirectionality(Directionality.ONE_DIRECTION);
			builder.setDirection("SE");
			break;
		case SOUTH_BOUND:
			builder.setDirectionality(Directionality.ONE_DIRECTION);
			builder.setDirection("S");
			break;
		case SOUTH_WEST_BOUND:
			builder.setDirectionality(Directionality.ONE_DIRECTION);
			builder.setDirection("SW");
			break;
		case WEST_BOUND:
			builder.setDirectionality(Directionality.ONE_DIRECTION);
			builder.setDirection("W");
			break;
		case NORTH_WEST_BOUND:
			builder.setDirectionality(Directionality.ONE_DIRECTION);
			builder.setDirection("NW");
			break;
		case OTHER:
		case UNKNOWN:
			break;
		default:
			break;
		}
	}

	private static class GroupOfLocationsContext {
		final SituationRecordContext parentContext;
		final GroupOfLocations groupOfLocations;

		GroupOfLocationsContext(SituationRecordContext parentContext, GroupOfLocations groupOfLocations) {
			super();
			this.parentContext = parentContext;
			this.groupOfLocations = groupOfLocations;
		}

		String getCountry() {
			try {
				return parentContext.parentContext.parentContext.country;
			} catch (Exception e) {
				return null;
			}
		}
	}

}
