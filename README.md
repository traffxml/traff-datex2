# Javadoc

Javadoc for `master` is at https://traffxml.gitlab.io/traff-datex2/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-datex2/javadoc/dev/.

# Development

To update the XML schema and rebuild related classes:

- Place all .xsd files in the xsd/ folder
- If you need to edit bindings, do so in xsd/bindings.xml
- Optionally do a test run (writing classes to /tmp) and verify everything works:
  `xjc -no-header -d /tmp -b xsd/binding.xml xsd`
- Delete the contents of the schemas/ folder (but leave the folder itself)
- Run xjc to regenerate the class hierarchy:
  `xjc -no-header -d schemas -b xsd/binding.xml xsd`

Caveats:

- Remove unused XSD files from the xsd/ folder, else they will clutter up your object hierarchy or cause
  class naming conflicts
- Be sure to run xjc with the -no-header option, else git will report lost of changes which are just
  timestamps in source files
- Be sure to empty schamas/ before rebuilding classes, as unused classes will not get deleted
