/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-datex2 library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.datex2.input;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.datex2.Datex2Helper;
import org.traffxml.lib.alertclocation.AlertC;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.input.DataSource;

import eu.datex2.schema._2._2_0.D2LogicalModel;

public class Datex2DataSource extends DataSource {
	/**
	 * @brief The logger for log output.
	 * 
	 * Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	/* The MethodHandles hack is OK here as long as we can’t support Android anyway */
	static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private static final String KEY_SOURCE_LT = "lt";
	private static final String KEY_SOURCE_LTDB = "ltdb";
	private String cc = null;
	private String ltn = null;
	private Date lastUpdate = null;

	public Datex2DataSource(String id, String url, Properties properties) {
		super(id, url, properties);
		String dbUrl = properties.getProperty(getPropertyKey(KEY_SOURCE_LTDB));
		AlertC.setDbUrl(dbUrl);
		String ccltnProp = properties.getProperty(getPropertyKey(KEY_SOURCE_LT));
		if (ccltnProp != null) {
			String[] ccltn = ccltnProp.split("\\.");
			if (ccltn.length > 0) {
				if (!ccltn[0].trim().isEmpty())
					cc = ccltn[0].trim();
				if ((ccltn.length > 1) && !ccltn[1].trim().isEmpty())
					ltn = ccltn[1].trim();
			}
		}
	}

	@Override
	public Date getLastUpdate() {
		return lastUpdate;
	}

	@Override
	public int getMinUpdateInterval() {
		return 0;
	}

	@Override
	public boolean needsExistingMessages() {
		return true;
	}

	@Override
	public Collection<TraffMessage> poll(Collection<TraffMessage> oldMessages, int pollInterval) {
		URL url = getUrl();
		D2LogicalModel d2lm = null;
		try {
			InputStream dataInputStream;
			URLConnection connection = url.openConnection();
			/* if content is gzipped, unzip it on the fly */
			if ("application/gzip".equals(connection.getContentType())
					|| "application/x-gzip".equals(connection.getContentType())
					|| "gzip".equals(connection.getContentEncoding()))
				dataInputStream = new GZIPInputStream(connection.getInputStream());
			else
				dataInputStream = connection.getInputStream();
			d2lm = Datex2Helper.getD2LogicalModel(dataInputStream);
			lastUpdate = new Date();
			return Datex2Helper.toTraff(d2lm, id, cc, ltn, pollInterval, oldMessages);
		} catch (MalformedURLException e) {
			LOG.debug("{}", e);
		} catch (IOException e) {
			LOG.debug("{}", e);
		}
		return null;
	}
}
