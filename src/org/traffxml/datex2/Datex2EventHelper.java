package org.traffxml.datex2;
/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml-datex2 library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.datex2.Datex2Helper.SituationRecordContext;
import org.traffxml.traff.DurationQuantifier;
import org.traffxml.traff.IntQuantifier;
import org.traffxml.traff.IntsQuantifier;
import org.traffxml.traff.Quantifier;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffSupplementaryInfo;
import eu.datex2.schema._2._2_0.AbnormalTraffic;
import eu.datex2.schema._2._2_0.Accident;
import eu.datex2.schema._2._2_0.AccidentTypeEnum;
import eu.datex2.schema._2._2_0.Activity;
import eu.datex2.schema._2._2_0.AnimalPresenceObstruction;
import eu.datex2.schema._2._2_0.AuthorityOperation;
import eu.datex2.schema._2._2_0.CarParks;
import eu.datex2.schema._2._2_0.Cause;
import eu.datex2.schema._2._2_0.Conditions;
import eu.datex2.schema._2._2_0.ConstructionWorks;
import eu.datex2.schema._2._2_0.Delays;
import eu.datex2.schema._2._2_0.DisturbanceActivity;
import eu.datex2.schema._2._2_0.EnvironmentalObstruction;
import eu.datex2.schema._2._2_0.EquipmentOrSystemFault;
import eu.datex2.schema._2._2_0.GeneralInstructionOrMessageToRoadUsers;
import eu.datex2.schema._2._2_0.GeneralNetworkManagement;
import eu.datex2.schema._2._2_0.GeneralObstruction;
import eu.datex2.schema._2._2_0.GenericSituationRecord;
import eu.datex2.schema._2._2_0.Impact;
import eu.datex2.schema._2._2_0.InfrastructureDamageObstruction;
import eu.datex2.schema._2._2_0.MaintenanceVehicleActionsEnum;
import eu.datex2.schema._2._2_0.MaintenanceVehicles;
import eu.datex2.schema._2._2_0.MaintenanceWorks;
import eu.datex2.schema._2._2_0.ManagedCause;
import eu.datex2.schema._2._2_0.NetworkManagement;
import eu.datex2.schema._2._2_0.NonManagedCause;
import eu.datex2.schema._2._2_0.NonRoadEventInformation;
import eu.datex2.schema._2._2_0.NonWeatherRelatedRoadConditionTypeEnum;
import eu.datex2.schema._2._2_0.NonWeatherRelatedRoadConditions;
import eu.datex2.schema._2._2_0.Obstruction;
import eu.datex2.schema._2._2_0.ObstructionTypeEnum;
import eu.datex2.schema._2._2_0.OperatorAction;
import eu.datex2.schema._2._2_0.PlacesEnum;
import eu.datex2.schema._2._2_0.PoorEnvironmentConditions;
import eu.datex2.schema._2._2_0.PoorEnvironmentTypeEnum;
import eu.datex2.schema._2._2_0.PublicEvent;
import eu.datex2.schema._2._2_0.ReroutingManagement;
import eu.datex2.schema._2._2_0.ReroutingManagementTypeEnum;
import eu.datex2.schema._2._2_0.RoadConditions;
import eu.datex2.schema._2._2_0.RoadMaintenanceTypeEnum;
import eu.datex2.schema._2._2_0.RoadOperatorServiceDisruption;
import eu.datex2.schema._2._2_0.RoadOrCarriagewayOrLaneManagement;
import eu.datex2.schema._2._2_0.RoadsideAssistance;
import eu.datex2.schema._2._2_0.RoadsideServiceDisruption;
import eu.datex2.schema._2._2_0.Roadworks;
import eu.datex2.schema._2._2_0.SignSetting;
import eu.datex2.schema._2._2_0.SpeedManagement;
import eu.datex2.schema._2._2_0.Subjects;
import eu.datex2.schema._2._2_0.TrafficElement;
import eu.datex2.schema._2._2_0.TrafficTypeEnum;
import eu.datex2.schema._2._2_0.TransitInformation;
import eu.datex2.schema._2._2_0.VehicleObstruction;
import eu.datex2.schema._2._2_0.WeatherRelatedRoadConditionTypeEnum;
import eu.datex2.schema._2._2_0.WeatherRelatedRoadConditions;
import eu.datex2.schema._2._2_0.WinterDrivingManagement;

public class Datex2EventHelper {
	/*
	 * TODO multiple items of information translating to a single event
	 * For example: a ConstructionWorks instance with type DEMOLITION_WORK and subject type BRIDGE translates
	 * to CONSTRUCTION_BRIDGE_DEMOLITION. Without the subject, the event might be CONSTRUCTION_CONSTRUCTION_WORK
	 * (due to lack of a generic demolition event); whereas the subject type in conjunction with a different
	 * event might translate to a supplementary information item S_PLACE_BRIDGE (in addition to the event).
	 */
	/*
	 * TODO handle “orphaned” quantifiers and SI items:
	 * Determine an event to attach them to (from a list of allowed events); if no matching event is found,
	 * either add a default event or ignore (varies from case to case).
	 */

	/**
	 * @brief The logger for log output.
	 * 
	 * Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	/** Datex-2 enum to TraFF event mapping */
	static Map<String, Set<TraffEventMapping>> ENUM_TO_EVENT = new HashMap<String, Set<TraffEventMapping>>();

	/** Datex-2 enum to TraFF SI mapping */
	static Map<String, Set<TraffSiMapping>> ENUM_TO_SI = new HashMap<String, Set<TraffSiMapping>>();
	static {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(Datex2EventHelper.class.getResourceAsStream("events.csv")));
			// first line contains column headers, skip
			br.readLine();
			String line;
			while ((line = br.readLine()) != null) {
				String[] fields = Datex2Helper.COLON_PATTERN.split(line);
				/* 
				 * Fields 0 (package), 1 (class) and 2 (enum value) are mandatory.
				 * Field 4 (event) is mandatory for events, 3 (event class) is ignored.
				 * Field 5 (speed), 6 (quantifier type) and 7 (quantifier value) are optional for
				 * events.
				 * Field 9 (Supplementary Information) is mandatory for supplementary information,
				 * field 8 (SI class) is ignored.
				 */
				if (fields.length < 5)
					continue;
				if (fields[0].isEmpty() || fields[1].isEmpty() || fields[2].isEmpty())
					continue;
				String key = String.format("%s.%s.%s", fields[0], fields[1], fields[2]);
				if (!fields[4].isEmpty()) {
					/* add event mapping */
					Set<TraffEventMapping> mappings = ENUM_TO_EVENT.get(key);
					TraffSiMapping siMapping = null;
					Integer speed = null;
					if ((fields.length >= 6) && !fields[5].isEmpty())
						try {
							speed = Integer.valueOf(fields[5]);
						} catch (Exception e) {
							LOG.debug("Invalid speed for {}.{}.{}: {}", fields[0], fields[1], fields[2], fields[5]);
						}
					if (mappings == null) {
						mappings = new HashSet<TraffEventMapping>();
						ENUM_TO_EVENT.put(key, mappings);
					}
					if ((fields.length >= 10) && !fields[9].isEmpty())
						/* add SI mapping to event mapping */
						try {
							siMapping = new TraffSiMapping(fields[9]);
						} catch (IllegalArgumentException e) {
							// NOP
						}
					try {
						mappings.add(new TraffEventMapping(fields[4], speed,
								(fields.length >= 7) ? fields[6] : null,
										(fields.length >= 8) ? fields[7] : null,
												siMapping));
					} catch (IllegalArgumentException e) {
						continue;
					}
				} else if ((fields.length >= 10) && !fields[9].isEmpty()) {
					/* add SI mapping */
					Set<TraffSiMapping> mappings = ENUM_TO_SI.get(key);
					if (mappings == null) {
						mappings = new HashSet<TraffSiMapping>();
						ENUM_TO_SI.put(key, mappings);
					}
					try {
						mappings.add(new TraffSiMapping(fields[9]));
					} catch (IllegalArgumentException e) {
						// NOP
					}
				}
			}
		} catch (IOException e) {
			LOG.debug("{}", e);
		}
	}

	static Collection<TraffEvent> getTraffEventsFromSituationRecord(SituationRecordContext context) {
		if (context.situationRecord.getCause() != null)
			addEventsFromCause(context, context.situationRecord.getCause());
		if (context.situationRecord.getImpact() != null)
			addEventsFromImpact(context, context.situationRecord.getImpact());
		if (context.situationRecord instanceof GenericSituationRecord) {
			// nothing useful here
		} else if (context.situationRecord instanceof NonRoadEventInformation) {
			addEventsFromNonRoadEventInformation(context, (NonRoadEventInformation) context.situationRecord);
		} else if (context.situationRecord instanceof OperatorAction) {
			addEventsFromOperatorAction(context, (OperatorAction) context.situationRecord);
		} else if (context.situationRecord instanceof TrafficElement) {
			addEventsFromTrafficElement(context, (TrafficElement) context.situationRecord);
		} else
			LOG.warn("Unsupported SituationRecord subclass in " + context.situationRecord.getId() +
					": " + context.situationRecord.getClass().getCanonicalName());
		if (context.getEvents().isEmpty() && context.getSi().isEmpty()) {
			LOG.warn("No matching events or SI for situation record {} of class {}",
					context.situationRecord.getId(), context.situationRecord.getClass().getCanonicalName());
		}
		return new ArrayList<TraffEvent>(context.getEvents());
	}

	private static void addEventsFromAbnormalTraffic(SituationRecordContext context, AbnormalTraffic abnormalTraffic) {
		if (abnormalTraffic.getAbnormalTrafficType() != null)
			addFromEnum(context, abnormalTraffic.getAbnormalTrafficType());
	    // TODO numberOfVehiclesWaiting
	    // TODO queueLength
		if (abnormalTraffic.getRelativeTrafficFlow() != null)
			addFromEnum(context, abnormalTraffic.getRelativeTrafficFlow());
		if (abnormalTraffic.getTrafficFlowCharacteristics() != null)
			addFromEnum(context, abnormalTraffic.getTrafficFlowCharacteristics());
		if (abnormalTraffic.getTrafficTrendType() != null)
			addFromEnum(context, abnormalTraffic.getTrafficTrendType());
		if (abnormalTraffic.getTrafficTrendType() != null)
			addFromEnum(context, abnormalTraffic.getTrafficTrendType());
	}

	private static void addEventsFromAccident(SituationRecordContext context, Accident accident) {
		Collection<TraffEvent> events = new HashSet<TraffEvent>();
		IntQuantifier quantifier = null;
		// accidentCause holds no useful information
		if (accident.getAccidentType() != null)
			for (AccidentTypeEnum type : accident.getAccidentType())
				events.addAll(getEventsFromEnum(type));
		// fall back to INCIDENT_ACCIDENT if nothing else matches
		if (events.isEmpty()) {
			TraffEvent.Builder builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.INCIDENT);
			builder.setType(TraffEvent.Type.INCIDENT_ACCIDENT);
			events.add(builder.build());
		}
		// totalNumberOfPeopleInvolved holds no useful information
		if (accident.getTotalNumberOfVehiclesInvolved() != null) {
			quantifier = new IntQuantifier(accident.getTotalNumberOfVehiclesInvolved().intValue());
			for (TraffEvent event : events) {
				TraffEvent.Builder builder = new TraffEvent.Builder(event);
				builder.setQuantifier(quantifier);
				context.addEvent(builder.build());
			}
		} else
			for (TraffEvent event : events)
				context.addEvent(event);
		// TODO vehicleInvolved
		// TODO groupOfVehiclesInvolved
		// groupOfPeopleInvolved holds no useful information
	}

	private static void addEventsFromActivity(SituationRecordContext context, Activity activity) {
		// TODO mobilityOfActivity
		if (activity.getClass().equals(Activity.class)) {
			// no useful information here
		} else if (activity instanceof AuthorityOperation) {
			addEventsFromAuthorityOperation(context, (AuthorityOperation) activity);
		} else if (activity instanceof DisturbanceActivity) {
			addEventsFromDisturbanceActivity(context, (DisturbanceActivity) activity);
		} else if (activity instanceof PublicEvent) {
			addEventsFromPublicEvent(context, (PublicEvent) activity);
		} else
			LOG.warn("Unsupported Activity subclass in " + context.situationRecord.getId() +
					": " + activity.getClass().getCanonicalName());
	}

	private static void addEventsFromAnimalPresenceObstruction(SituationRecordContext context,
			AnimalPresenceObstruction animalPresenceObstruction) {
		Collection<TraffEvent> events = new HashSet<TraffEvent>();
		if (animalPresenceObstruction.getAnimalPresenceType() != null)
			events.addAll(getEventsFromEnum(animalPresenceObstruction.getAnimalPresenceType()));
		if (events.isEmpty()) {
			// TODO fall back to HAZARD_ANIMALS_ON_ROAD if nothing else matches
		} else
			for (TraffEvent event : events)
				context.addEvent(event);
	}

	private static void addEventsFromAuthorityOperation(SituationRecordContext context, AuthorityOperation authorityOperation) {
		if (authorityOperation.getAuthorityOperationType() != null)
			addFromEnum(context, authorityOperation.getAuthorityOperationType());
	}

	private static void addEventsFromCause(SituationRecordContext context, Cause cause) {
		if (cause instanceof ManagedCause) {
			// nothing useful here
		} else if (cause instanceof NonManagedCause) {
			addEventsFromNonManagedCause(context, (NonManagedCause) cause);
		} else
			LOG.warn("Unsupported Cause subclass in " + context.situationRecord.getId() +
					": " + cause.getClass().getCanonicalName());
	}

	private static void addEventsFromConditions(SituationRecordContext context, Conditions conditions) {
		if (conditions.getDrivingConditionType() != null)
			addFromEnum(context, conditions.getDrivingConditionType());
		if (conditions.getClass().equals(Conditions.class)) {
			// no useful information here
		} else if (conditions instanceof PoorEnvironmentConditions) {
			addEventsFromPoorEnvironmentConditions(context, (PoorEnvironmentConditions) conditions);
		} else if (conditions instanceof RoadConditions) {
			addEventsFromRoadConditions(context, (RoadConditions) conditions);
		} else if (conditions.getClass() != Conditions.class)
			LOG.warn("Unsupported Conditions subclass in " + context.situationRecord.getId() +
					": " + conditions.getClass().getCanonicalName());
	}

	private static void addEventsFromConstructionWorks(SituationRecordContext context, ConstructionWorks constructionWorks) {
		if (constructionWorks.getConstructionWorkType() != null)
			addFromEnum(context, constructionWorks.getConstructionWorkType());
	}

	private static void addEventsFromDelays(SituationRecordContext context, Delays delays) {
		TraffEvent delayEvent = null;
		Collection<TraffEvent> delayEvents = getEventsFromEnum(delays.getDelaysType());
		for (TraffEvent newEvent : delayEvents)
			delayEvent = newEvent;

		delayEvents = getEventsFromEnum(delays.getDelayBand());
		for (TraffEvent newEvent : delayEvents)
			if (delayEvent == null)
				delayEvent = newEvent;
			else if (newEvent.quantifier != null) {
				TraffEvent.Builder builder = new TraffEvent.Builder(delayEvent);
				builder.setQuantifier(newEvent.quantifier);
				delayEvent = builder.build();
			}

		if (delays.getDelayTimeValue() != null) {
			TraffEvent.Builder builder;
			if (delayEvent != null)
				builder = new TraffEvent.Builder(delayEvent);
			else {
				builder = new TraffEvent.Builder();
				builder.setEventClass(TraffEvent.Class.DELAY);
				builder.setType(TraffEvent.Type.DELAY_DELAY);
			}
			builder.setQuantifier(new DurationQuantifier((int) (delays.getDelayTimeValue() / 60)));
			delayEvent = builder.build();
		}

		if (delayEvent != null)
			context.addEvent(delayEvent);
	}

	private static void addEventsFromDisturbanceActivity(SituationRecordContext context, DisturbanceActivity disturbanceActivity) {
		if (disturbanceActivity.getDisturbanceActivityType() != null)
			addFromEnum(context, disturbanceActivity.getDisturbanceActivityType());
	}

	/**
	 * @brief Translates an enumeration to TraFF events and adds them to a collection.
	 * 
	 * This method is called for a variety of enums. Calling this method with an unsupported enum is a no-op.
	 * 
	 * @param events The collection to which the events will be added
	 * @param en The enum
	 */
	private static void addEventsFromEnum(SituationRecordContext context, Enum en) {
		if (en == null)
			return;
		Collection<TraffEvent> events = getEventsFromEnum(en);
		for (TraffEvent event : events)
			context.addEvent(event);
	}

	private static void addEventsFromEnvironmentalObstruction(SituationRecordContext context,
			EnvironmentalObstruction environmentalObstruction) {
		// TODO depth
		if (environmentalObstruction.getEnvironmentalObstructionType() != null)
			addFromEnum(context, environmentalObstruction.getEnvironmentalObstructionType());
	}

	private static void addEventsFromEquipmentOrSystemFault(SituationRecordContext context,
			EquipmentOrSystemFault equipmentOrSystemFault) {
		/*
		 * Events for this class depend on two enums, hence the usual (single) enum lookup does not work.
		 * Instead we are taking advantage of TraFF enum literals for this class being quite predictable:
		 * they take the form EQUIPMENT_STATUS_$X_$Y, where $X is the equipment type and $Y is the status.
		 * We therefore guess the literal and see if it exists; if it does, we add the matching event.
		 */
		if ((equipmentOrSystemFault.getEquipmentOrSystemFaultType() == null)
				|| (equipmentOrSystemFault.getFaultyEquipmentOrSystemType() == null))
			return;

		String faultType = null;
		switch (equipmentOrSystemFault.getEquipmentOrSystemFaultType()) {
		case NOT_WORKING:
		case OUT_OF_SERVICE:
			faultType = "NOT_WORKING";
			break;
		case WORKING_INCORRECTLY:
			faultType = "WORKING_INCORRECTLY";
			break;
			// TODO WORKING_INTERMITTENTLY
		default:
			break;
		}
		if (faultType == null)
			return;

		String faultyEquipment = null;
		switch (equipmentOrSystemFault.getFaultyEquipmentOrSystemType()) {
		// TODO ANPR_CAMERAS
		case AUTOMATED_TOLL_SYSTEM:
			faultyEquipment = "AUTOMATIC_TOLL_SYSTEM";
			break;
			// TODO CCTV_CAMERAS
		case EMERGENCY_ROADSIDE_TELEPHONES:
			faultyEquipment = "EMERGENCY_PHONES";
			break;
			// TODO GALLERY_LIGHTS
		case LANE_CONTROL_SIGNS:
			faultyEquipment = "LANE_CONTROL_SIGNS";
			break;
		case LEVEL_CROSSING:
			faultyEquipment = "LEVEL_CROSSING";
			break;
			// TODO MATRIX_SIGNS
		case RAMP_CONTROLS:
			faultyEquipment = "RAMP_SIGNALS";
			break;
			// TODO ROADSIDE_COMMUNICATIONS_SYSTEM
			// TODO ROADSIDE_POWER_SYSTEM
			// TODO SPEED_CONTROL_SIGNS
			// TODO STREET_LIGHTING
		case TEMPORARY_TRAFFIC_LIGHTS:
			faultyEquipment = "TEMPORARY_TRAFFIC_LIGHTS";
			break;
			// TODO TOLL_GATES
		case TRAFFIC_LIGHT_SETS:
		case TRAFFIC_SIGNALS:
			faultyEquipment = "TRAFFIC_LIGHTS";
			break;
			// TODO TUNNEL_LIGHTS
		case TUNNEL_VENTILATION:
			faultyEquipment = "TUNNEL_VENTILATION";
			// TODO VARIABLE_MESSAGE_SIGNS
			// TODO OTHER
		default:
			break;
		}
		if (faultyEquipment == null)
			return;

		try {
			TraffEvent.Builder builder = new TraffEvent.Builder();
			builder.setType(Enum.valueOf(TraffEvent.Type.class,
					String.format("%s_%s_%s", TraffEvent.Class.EQUIPMENT_STATUS.name(),
							faultyEquipment, faultType)));
			builder.setEventClass(TraffEvent.Class.EQUIPMENT_STATUS);
			context.addEvent(builder.build());
		} catch (IllegalArgumentException e) {
			// no event matching the name
			LOG.warn("{}_{}_{} is not a valid TraFF event, skipping",
					TraffEvent.Class.EQUIPMENT_STATUS.name(), faultyEquipment, faultType);
			return;
		}
	}

	private static void addEventsFromGeneralNetworkManagement(SituationRecordContext context,
			GeneralNetworkManagement generalNetworkManagement) {
		addFromEnum(context, generalNetworkManagement.getGeneralNetworkManagementType());
	}

	private static void addEventsFromGeneralInstructionOrMessageToRoadUsers(SituationRecordContext context,
			GeneralInstructionOrMessageToRoadUsers generalInstructionOrMessageToRoadUsers) {
		if (generalInstructionOrMessageToRoadUsers.getGeneralInstructionToRoadUsersType() != null)
			addFromEnum(context, generalInstructionOrMessageToRoadUsers.getGeneralInstructionToRoadUsersType());
	}

	private static void addEventsFromGeneralObstruction(SituationRecordContext context,
			GeneralObstruction generalObstruction) {
	    // TODO mobilityOfObstruction
		Collection<TraffEvent> events = new HashSet<TraffEvent>();
		if (generalObstruction.getObstructionType() != null)
			for (ObstructionTypeEnum type : generalObstruction.getObstructionType())
				events.addAll(getEventsFromEnum(type));
		if ((generalObstruction.getNumberOfObstructions() != null) && !events.isEmpty()) {
			IntQuantifier quantifier = new IntQuantifier(generalObstruction.getNumberOfObstructions().intValue());
			for (TraffEvent event : events) {
				TraffEvent.Builder builder = new TraffEvent.Builder(event);
				builder.setQuantifier(quantifier);
				context.addEvent(builder.build());
			}
		} else
			for (TraffEvent event : events)
				context.addEvent(event);
		// groupOfPeopleInvolved is irrelevant
	}

	private static void addEventsFromImpact(SituationRecordContext context, Impact impact) {
		if ((impact.getNumberOfLanesRestricted() != null) || (impact.getNumberOfOperationalLanes() != null)) {
			TraffEvent.Builder builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			if (impact.getNumberOfOperationalLanes() == null) {
				builder.setType(TraffEvent.Type.RESTRICTION_LANE_CLOSED);
				builder.setQuantifier(new IntQuantifier(impact.getNumberOfLanesRestricted().intValue()));
			} else {
				builder.setType(TraffEvent.Type.RESTRICTION_REDUCED_LANES);
				if (impact.getNumberOfLanesRestricted() != null)
					builder.setQuantifier(new IntsQuantifier(new int[]{
							impact.getNumberOfLanesRestricted().intValue() + impact.getNumberOfOperationalLanes().intValue(),
							impact.getNumberOfOperationalLanes().intValue()}));
				else
					builder.setQuantifier(new IntsQuantifier(new int[]{impact.getNumberOfOperationalLanes().intValue()}));
			}
			context.addEvent(builder.build());
		}
		addFromEnum(context, impact.getTrafficConstrictionType());
		if (impact.getDelays() != null)
			addEventsFromDelays(context, impact.getDelays());
		if ((impact.getNumberOfLanesRestricted() == null) && (impact.getNumberOfOperationalLanes() == null)
				&& (impact.getTrafficConstrictionType() == null) && (impact.getDelays() == null)) {
			/*
			 * TODO this is not a perfect solution yet
			 * As long as TraFF has no expression for residual capacity and road width, express
			 * them through the generic event RESTRICTION_RESTRICTIONS; ignore them if other Impact
			 * attributes are present.
			 */
			if (((impact.getCapacityRemaining() != null) && (impact.getCapacityRemaining() < 100.0f))
					|| (impact.getResidualRoadWidth() != null)) {
				TraffEvent.Builder builder = new TraffEvent.Builder();
				builder.setEventClass(TraffEvent.Class.RESTRICTION);
				builder.setType(TraffEvent.Type.RESTRICTION_RESTRICTIONS);
				context.addEvent(builder.build());
			}
		}
	}

	private static void addEventsFromInfrastructureDamageObstruction(SituationRecordContext context,
			InfrastructureDamageObstruction infrastructureDamageObstruction) {
		if (infrastructureDamageObstruction.getInfrastructureDamageType() != null)
			addFromEnum(context, infrastructureDamageObstruction.getInfrastructureDamageType());
	}

	private static void addEventsFromMaintenanceVehicleActions(SituationRecordContext context,
			List<MaintenanceVehicleActionsEnum> maintenanceVehicleActions) {
		for (MaintenanceVehicleActionsEnum action : maintenanceVehicleActions)
			addFromEnum(context, action);
	}

	private static void addEventsFromMaintenanceVehicles(SituationRecordContext context, MaintenanceVehicles maintenanceVehicles) {
		// TODO numberOfMaintenanceVehicles
		if (maintenanceVehicles.getMaintenanceVehicleActions() != null)
			addEventsFromMaintenanceVehicleActions(context, maintenanceVehicles.getMaintenanceVehicleActions());
	}

	private static void addEventsFromMaintenanceWorks(SituationRecordContext context, MaintenanceWorks maintenanceWorks) {
		if (maintenanceWorks.getRoadMaintenanceType() != null)
			for (RoadMaintenanceTypeEnum type : maintenanceWorks.getRoadMaintenanceType())
				addFromEnum(context, type);
	}

	private static void addEventsFromNetworkManagement(SituationRecordContext context, NetworkManagement networkManagement) {
		// TODO complianceOption?
		// TODO DirectionEnum?
		if (networkManagement.getApplicableForTrafficType() != null)
			for (TrafficTypeEnum trafficType : networkManagement.getApplicableForTrafficType())
				addSiFromEnum(context, trafficType);
		if (networkManagement.getPlacesAtWhichApplicable() != null)
			for (PlacesEnum place : networkManagement.getPlacesAtWhichApplicable())
				addSiFromEnum(context, place);
		// TODO VehicleCharacteristics (SI)
		if (networkManagement.getClass().equals(NetworkManagement.class)) {
			// no useful information here
		} else if (networkManagement instanceof GeneralInstructionOrMessageToRoadUsers) {
			addEventsFromGeneralInstructionOrMessageToRoadUsers(context, (GeneralInstructionOrMessageToRoadUsers) networkManagement);
		} else if (networkManagement instanceof GeneralNetworkManagement) {
			addEventsFromGeneralNetworkManagement(context, (GeneralNetworkManagement) networkManagement);
		} else if (networkManagement instanceof ReroutingManagement) {
			addEventsFromReroutingManagement(context, (ReroutingManagement) networkManagement);
		} else if (networkManagement instanceof RoadOrCarriagewayOrLaneManagement) {
			addEventsFromRoadOrCarriagewayOrLaneManagement(context, (RoadOrCarriagewayOrLaneManagement) networkManagement);
		} else if (networkManagement instanceof SpeedManagement) {
			addEventsFromSpeedManagement(context, (SpeedManagement) networkManagement);
		} else if (networkManagement instanceof WinterDrivingManagement) {
			// TODO
			LOG.warn("TODO SituationRecord subclass in " + context.situationRecord.getId() +
					" is not supported yet: " + networkManagement.getClass().getCanonicalName());
		} else
			LOG.warn("Unsupported NetworkManagement subclass in " + context.situationRecord.getId() +
					": " + networkManagement.getClass().getCanonicalName());
	}

	private static void addEventsFromNonManagedCause(SituationRecordContext context, NonManagedCause nonManagedCause) {
		addFromEnum(context, nonManagedCause.getCauseType());
	}

	private static void addEventsFromNonRoadEventInformation(SituationRecordContext context,
			NonRoadEventInformation nonRoadEventInformation) {
		// NonRoadEventInformation itself holds no useful information
		if (nonRoadEventInformation.getClass().equals(NonRoadEventInformation.class)) {
			// no useful information here
		} else if (nonRoadEventInformation instanceof CarParks) {
			// TODO
			LOG.warn("TODO SituationRecord subclass in " + context.situationRecord.getId() +
					" is not supported yet: " + nonRoadEventInformation.getClass().getCanonicalName());
		} else if (nonRoadEventInformation instanceof RoadOperatorServiceDisruption) {
			// TODO
			LOG.warn("TODO SituationRecord subclass in " + context.situationRecord.getId() +
					" is not supported yet: " + nonRoadEventInformation.getClass().getCanonicalName());
		} else if (nonRoadEventInformation instanceof RoadsideServiceDisruption) {
			// TODO
			LOG.warn("TODO SituationRecord subclass in " + context.situationRecord.getId() +
					" is not supported yet: " + nonRoadEventInformation.getClass().getCanonicalName());
		} else if (nonRoadEventInformation instanceof TransitInformation) {
			addEventsFromTransitInformation(context, (TransitInformation) nonRoadEventInformation);
		} else
			LOG.warn("Unsupported NonRoadEventInformation subclass in " + context.situationRecord.getId() +
					": " + nonRoadEventInformation.getClass().getCanonicalName());
	}

	private static void addEventsFromNonWeatherRelatedRoadConditions(SituationRecordContext context,
			NonWeatherRelatedRoadConditions nonWeatherRelatedRoadConditions) {
		if (nonWeatherRelatedRoadConditions.getNonWeatherRelatedRoadConditionType() != null)
			for (NonWeatherRelatedRoadConditionTypeEnum type : nonWeatherRelatedRoadConditions.getNonWeatherRelatedRoadConditionType())
				addFromEnum(context, type);
	}

	private static void addEventsFromObstruction(SituationRecordContext context, Obstruction obstruction) {
	    // TODO numberOfObstructions (unless handled in subclass)
	    // TODO mobilityOfObstruction (unless handled in subclass)
		if (obstruction.getClass().equals(Obstruction.class)) {
			// no useful information here
		} else if (obstruction instanceof AnimalPresenceObstruction) {
			addEventsFromAnimalPresenceObstruction(context, (AnimalPresenceObstruction) obstruction);
		} else if (obstruction instanceof EnvironmentalObstruction) {
			addEventsFromEnvironmentalObstruction(context, (EnvironmentalObstruction) obstruction);
		} else if (obstruction instanceof GeneralObstruction) {
			addEventsFromGeneralObstruction(context, (GeneralObstruction) obstruction);
		} else if (obstruction instanceof InfrastructureDamageObstruction) {
			addEventsFromInfrastructureDamageObstruction(context, (InfrastructureDamageObstruction) obstruction);
		} else if (obstruction instanceof VehicleObstruction) {
			addEventsFromVehicleObstruction(context, (VehicleObstruction) obstruction);
		} else
			LOG.warn("Unsupported Obstruction subclass in " + context.situationRecord.getId() +
					": " + obstruction.getClass().getCanonicalName());
	}

	private static void addEventsFromOperatorAction(SituationRecordContext context, OperatorAction operatorAction) {
		// OperatorAction itself holds no useful information
		if (operatorAction.getClass().equals(OperatorAction.class)) {
			// no useful information here
		} else if (operatorAction instanceof NetworkManagement) {
			addEventsFromNetworkManagement(context, (NetworkManagement) operatorAction);
		} else if (operatorAction instanceof RoadsideAssistance) {
			// no useful information here
		} else if (operatorAction instanceof Roadworks) {
			addEventsFromRoadworks(context, (Roadworks) operatorAction);
		} else if (operatorAction instanceof SignSetting) {
			// no useful information here
		} else
			LOG.warn("Unsupported OperatorAction subclass in " + context.situationRecord.getId() +
					": " + operatorAction.getClass().getCanonicalName());
	}

	private static void addEventsFromPoorEnvironmentConditions(SituationRecordContext context,
			PoorEnvironmentConditions poorEnvironmentConditions) {
		if (poorEnvironmentConditions.getPoorEnvironmentType() != null)
			for (PoorEnvironmentTypeEnum type : poorEnvironmentConditions.getPoorEnvironmentType())
				addFromEnum(context, type);
		// TODO precipitationDetail
		// TODO visibility
		// TODO pollution
		// TODO temperature
		// TODO wind
		// TODO humidity
	}

	private static void addEventsFromPublicEvent(SituationRecordContext context, PublicEvent publicEvent) {
		// TODO mobilityOfActivity (inherited)
		if (publicEvent.getPublicEventType() != null)
			addFromEnum(context, publicEvent.getPublicEventType());
	}

	private static void addEventsFromReroutingManagement(SituationRecordContext context,
			ReroutingManagement reroutingManagement) {
		if (reroutingManagement.getReroutingManagementType() != null)
			for (ReroutingManagementTypeEnum type : reroutingManagement.getReroutingManagementType())
				addFromEnum(context, type);
		if (Boolean.TRUE.equals(reroutingManagement.isSignedRerouting()))
			context.addSi(new TraffSupplementaryInfo(TraffSupplementaryInfo.Class.DIVERSION,
					TraffSupplementaryInfo.Type.S_DIVERSION_FOLLOW_DIVERSION_SIGNS));
		// alternativeRoute, entry, exit, roadOrJunctionNumber are not supported in TraFF
	}

	private static void addEventsFromRoadConditions(SituationRecordContext context, RoadConditions roadConditions) {
		// RoadConditions holds no useful information
		if (roadConditions.getClass().equals(RoadConditions.class)) {
			// no useful information here
		} else if (roadConditions instanceof NonWeatherRelatedRoadConditions) {
			addEventsFromNonWeatherRelatedRoadConditions(context, (NonWeatherRelatedRoadConditions) roadConditions);
		} else if (roadConditions instanceof WeatherRelatedRoadConditions) {
			addEventsFromWeatherRelatedRoadConditions(context, (WeatherRelatedRoadConditions) roadConditions);
		} else
			LOG.warn("Unsupported RoadConditions subclass in " + context.situationRecord.getId() +
					": " + roadConditions.getClass().getCanonicalName());
	}

	private static void addEventsFromRoadOrCarriagewayOrLaneManagement(SituationRecordContext context,
			RoadOrCarriagewayOrLaneManagement roadOrCarriagewayOrLaneManagement) {
		addFromEnum(context, roadOrCarriagewayOrLaneManagement.getRoadOrCarriagewayOrLaneManagementType());
	}

	private static void addEventsFromRoadworks(SituationRecordContext context, Roadworks roadworks) {
		if (roadworks.getRoadworksDuration() != null)
			addFromEnum(context, roadworks.getRoadworksDuration());
		// TODO expiration time from RoadworksDuration?
		// TODO Mobility (mobile = slow-moving maintenance vehicles in some or all cases)
		if (roadworks.getSubjects() != null)
			addEventsFromSubjects(context, roadworks.getSubjects());
		if (roadworks.getMaintenanceVehicles() != null)
			addEventsFromMaintenanceVehicles(context, roadworks.getMaintenanceVehicles());
		if (roadworks.getClass().equals(Roadworks.class)) {
			// no useful information here
		} else if (roadworks instanceof ConstructionWorks) {
			addEventsFromConstructionWorks(context, (ConstructionWorks) roadworks);
		} else if (roadworks instanceof MaintenanceWorks) {
			addEventsFromMaintenanceWorks(context, (MaintenanceWorks) roadworks);
		} else
			LOG.warn("Unsupported Roadworks subclass in " + context.situationRecord.getId() +
					": " + roadworks.getClass().getCanonicalName());
	}

	private static void addEventsFromSpeedManagement(SituationRecordContext context,
			SpeedManagement speedManagement) {
		Collection<TraffEvent> events = new HashSet<TraffEvent>();
		if (speedManagement.getSpeedManagementType() != null)
			events = getEventsFromEnum(speedManagement.getSpeedManagementType());
		if (speedManagement.getTemporarySpeedLimit() != null) {
			if (events.isEmpty()) {
				TraffEvent.Builder builder = new TraffEvent.Builder();
				builder.setEventClass(TraffEvent.Class.RESTRICTION);
				builder.setType(TraffEvent.Type.RESTRICTION_SPEED_LIMIT);
				builder.setSpeed(speedManagement.getTemporarySpeedLimit().intValue());
				context.addEvent(builder.build());
			} else
				for (TraffEvent event : events) {
					TraffEvent.Builder builder = new TraffEvent.Builder(event);
					builder.setSpeed(speedManagement.getTemporarySpeedLimit().intValue());
					context.addEvent(builder.build());
				}
		} else
			for (TraffEvent event : events)
				context.addEvent(event);
	}

	private static void addEventsFromSubjects(SituationRecordContext context, Subjects subjects) {
		// TODO numberOfSubjects
		addFromEnum(context, subjects.getSubjectTypeOfWorks());
	}

	private static void addEventsFromTrafficElement(SituationRecordContext context, TrafficElement trafficElement) {
		// TrafficElement itself holds no useful information
		if (trafficElement.getClass().equals(TrafficElement.class)) {
			// no useful information here
		} else if (trafficElement instanceof AbnormalTraffic) {
			addEventsFromAbnormalTraffic(context, (AbnormalTraffic) trafficElement);
		} else if (trafficElement instanceof Accident) {
			addEventsFromAccident(context, (Accident) trafficElement);
		} else if (trafficElement instanceof Activity) {
			addEventsFromActivity(context, (Activity) trafficElement);
		} else if (trafficElement instanceof Conditions) {
			addEventsFromConditions(context, (Conditions) trafficElement);
		} else if (trafficElement instanceof EquipmentOrSystemFault) {
			addEventsFromEquipmentOrSystemFault(context, (EquipmentOrSystemFault) trafficElement);
		} else if (trafficElement instanceof Obstruction) {
			addEventsFromObstruction(context, (Obstruction) trafficElement);
		} else
			LOG.warn("Unsupported TrafficElement subclass in " + context.situationRecord.getId() +
					": " + trafficElement.getClass().getCanonicalName());
	}

	private static void addEventsFromTransitInformation(SituationRecordContext context,
			TransitInformation transitInformation) {
	    // TODO scheduledDepartureTime
		/* TODO the necessary events are not defined yet
		TraffEvent.Type type = null;
		if (TransitServiceInformationEnum.SERVICE_NOT_OPERATING.equals(transitInformation.getTransitServiceInformation())
				&& transitInformation.getTransitServiceType() != null)
			switch(transitInformation.getTransitServiceType()) {
			case BUS:
				type = TraffEvent.Type.TRANSIT_NO_BUS_SERVICE;
				break;
			case FERRY:
				type = TraffEvent.Type.TRANSIT_NO_FERRY_SERVICE;
				break;
			case RAIL:
				type = TraffEvent.Type.TRANSIT_NO_RAIL_SERVICE;
				break;
			case UNDERGROUND_METRO:
				type = TraffEvent.Type.TRANSPORT_NO_UNDERGROUND_SERVICE;
				break;
			default:
				break;
			}
		if (type != null) {
			TraffEvent.Builder builder = new TraffEvent.Builder();
			builder.setType(type);
			builder.setEventClass(getTraffEventClass(type));
			context.addEvent(builder.build());
		} else
		*/
			addFromEnum(context, transitInformation.getTransitServiceInformation());
	}

	private static void addEventsFromVehicleObstruction(SituationRecordContext context,
			VehicleObstruction vehicleObstruction) {
		if (vehicleObstruction.getVehicleObstructionType() != null)
			addFromEnum(context, vehicleObstruction.getVehicleObstructionType());
		// TODO obstructingVehicle
	}

	private static void addEventsFromWeatherRelatedRoadConditions(SituationRecordContext context,
			WeatherRelatedRoadConditions weatherRelatedRoadConditions) {
		if (weatherRelatedRoadConditions.getWeatherRelatedRoadConditionType() != null)
			for (WeatherRelatedRoadConditionTypeEnum type : weatherRelatedRoadConditions.getWeatherRelatedRoadConditionType())
				addFromEnum(context, type);
		// TODO roadSurfaceConditionMeasurements
	}

	/**
	 * @brief Translates an enumeration to TraFF events and SI items and adds them to the situation record context.
	 * 
	 * This method is called for a variety of enums. Calling this method with an unsupported enum is a no-op.
	 * 
	 * @param context The context to which the TraFF events and SI items will be added
	 * @param en The enum
	 */
	private static void addFromEnum(SituationRecordContext context, Enum en) {
		addEventsFromEnum(context, en);
		addSiFromEnum(context, en);
	}

	/**
	 * @brief Translates an enumeration to TraFF SI items and adds them to the situation record context.
	 * 
	 * This method is called for a variety of enums. Calling this method with an unsupported enum is a no-op.
	 * 
	 * @param context The context to which the SI items will be added
	 * @param en The enum
	 */
	private static void addSiFromEnum(SituationRecordContext context, Enum en) {
		if (en == null)
			return;
		Collection<TraffSupplementaryInfo> sis = getSiFromEnum(en);
		for (TraffSupplementaryInfo si : sis)
			context.addSi(si);
	}

	private static Collection<TraffEvent> getEventsFromEnum(Enum en) {
		Collection<TraffEvent> res = new HashSet<TraffEvent>();
		if (en == null)
			return res;
		String key = String.format("%s.%s", en.getClass().getCanonicalName(), en.name());
		Set<TraffEventMapping> mappings = ENUM_TO_EVENT.get(key);
		if (mappings == null)
			return res;
		for (TraffEventMapping mapping : mappings) {
			TraffEvent.Builder builder = new TraffEvent.Builder();
			builder.setType(mapping.traffEvent);
			builder.setEventClass(mapping.traffClass);
			builder.setSpeed(mapping.speed);
			builder.setQuantifier(mapping.quantifier);
			if (mapping.siMapping != null)
				builder.addSupplementaryInfo(new TraffSupplementaryInfo(mapping.siMapping.siClass,
						mapping.siMapping.siType));
			res.add(builder.build());
		}
		return res;
	}

	private static Collection<TraffSupplementaryInfo> getSiFromEnum(Enum en) {
		Collection<TraffSupplementaryInfo> res = new HashSet<TraffSupplementaryInfo>();
		if (en == null)
			return res;
		String key = String.format("%s.%s", en.getClass().getCanonicalName(), en.name());
		Set<TraffSiMapping> mappings = ENUM_TO_SI.get(key);
		if (mappings == null)
			return res;
		for (TraffSiMapping mapping : mappings)
			res.add(new TraffSupplementaryInfo(mapping.siClass, mapping.siType));
		return res;
	}

	/**
	 * @brief Determines the TraFF event class for a given TraFF event type.
	 * 
	 * This is done based on the convention that event types begin with the event class, followed by an
	 * underscore. If the string representation of the event type does not match that of a known event class,
	 * the last underscore and all characters following it are dropped. This is repeated until a match is
	 * found or the remaining string does not contain any more underscores. The first match is returned,
	 * which is equivalent to the “maximum munch” rule: If we were to have an event class named {@code FOO}
	 * and another named {@code FOO_BAR}, then an event type {@code FOO_BAR_BAZ} would yield {@code FOO_BAR},
	 * not {@code FOO}.
	 * 
	 * @param type The event type
	 * @return The event class, or null if no match was found (including if {@code type} is null)
	 */
	private static TraffEvent.Class getTraffEventClass(TraffEvent.Type type) {
		if (type == null)
			return null;
		TraffEvent.Class res = null;
		String name = type.name();
		while (res == null) {
			try {
				res = Enum.valueOf(TraffEvent.Class.class, name);
				break;
			} catch (IllegalArgumentException e) {
				if (!name.contains("_"))
					break;
				name = name.substring(0, name.lastIndexOf("_"));
			}
		}
		return res;
	}

	/**
	 * @brief Determines the TraFF supplementary information class for a given TraFF SI type.
	 * 
	 * This is done based on the convention that SI types begin with {@code S_}, followed by the event class
	 * and an underscore. If the string representation of the event type does not match that of a known event
	 * class, the last underscore and all characters following it are dropped. This is repeated until a match
	 * is found or the remaining string does not contain any more underscores. The first match is returned,
	 * which is equivalent to the “maximum munch” rule: If we were to have an event class named {@code FOO}
	 * and another named {@code FOO_BAR}, then an event type {@code FOO_BAR_BAZ} would yield {@code FOO_BAR},
	 * not {@code FOO}.
	 * 
	 * @param type The event type
	 * @return The event class, or null if no match was found (including if {@code type} is null)
	 */
	private static TraffSupplementaryInfo.Class getTraffSiClass(TraffSupplementaryInfo.Type type) {
		if (type == null)
			return null;
		String name = type.name();
		if (name.startsWith("S_"))
			name = name.substring(2);
		else
			return null;
		TraffSupplementaryInfo.Class res = null;
		while (res == null) {
			try {
				res = Enum.valueOf(TraffSupplementaryInfo.Class.class, name);
				break;
			} catch (IllegalArgumentException e) {
				if (!name.contains("_"))
					break;
				name = name.substring(0, name.lastIndexOf("_"));
			}
		}
		return res;
	}

	/**
	 * A mapping which specifies how a Datex-2 Enum value maps to one or multiple TraFF events.
	 * 
	 * If a single value is represented by multiple TraFF events, one {@code TraffEventMapping} instance
	 * exists for each TraFF event.
	 */
	private static class TraffEventMapping {
		static final Pattern TIME_PATTERN = Pattern.compile(":");

		/** The TraFF event class */
		public final TraffEvent.Class traffClass;

		/** The TraFF event */
		public final TraffEvent.Type traffEvent;

		/** The speed limit, or speed of flowing traffic */
		public final Integer speed;

		/** The quantifier type, null for no quantifier */
		public final Quantifier quantifier;

		/** Supplementary information mapping */
		public final TraffSiMapping siMapping;

		TraffEventMapping(String traffEvent, Integer speed, String qType, String quantifier, TraffSiMapping siMapping)
				throws IllegalArgumentException {
			this.traffEvent = Enum.valueOf(TraffEvent.Type.class, traffEvent);
			this.traffClass = getTraffEventClass(this.traffEvent);
			this.speed = speed;
			this.siMapping = siMapping;
			if (qType == null)
				this.quantifier = null;
			else if ("duration".equals(qType)) {
				String[] durationString = TIME_PATTERN.split(quantifier);
				Integer duration = null;
				switch (durationString.length) {
				case 1:
					duration = Integer.valueOf(durationString[0]);
					break;
				case 2:
					duration = Integer.valueOf(durationString[0]) * 60 + Integer.valueOf(durationString[1]);
					break;
				case 3:
					duration = Integer.valueOf(durationString[0]) * 60 + Integer.valueOf(durationString[1]);
					if (Integer.valueOf(durationString[1]) >= 30)
						duration++;
					break;
				default:
					throw new IllegalArgumentException("Illegal duration: '" + quantifier + "'");
				}
				this.quantifier = new DurationQuantifier(duration);
			} else
				// TODO add support for more quantifier types
				throw new IllegalArgumentException("Unsupported quantifier type: '" + qType + "'");
		}
	}

	/**
	 * A mapping which specifies how a Datex-2 Enum value maps to one or multiple TraFF supplementary information items.
	 * 
	 * If a single value is represented by multiple TraFF SI items, one {@code TraffSiMapping} instance
	 * exists for each TraFF SI item.
	 */
	private static class TraffSiMapping {

		/** The TraFF supplementary information class */
		public final TraffSupplementaryInfo.Class siClass;

		/** The TraFF supplementary information type */
		public final TraffSupplementaryInfo.Type siType;

		TraffSiMapping(String type) throws IllegalArgumentException {
			super();
			this.siType = Enum.valueOf(TraffSupplementaryInfo.Type.class, type);
			this.siClass = getTraffSiClass(this.siType);
		}
	}
}
