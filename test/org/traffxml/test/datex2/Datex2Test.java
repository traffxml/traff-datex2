package org.traffxml.test.datex2;
/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-datex2 library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.traffxml.datex2.Datex2Helper;
import org.traffxml.lib.alertclocation.AlertC;
import org.traffxml.lib.alertclocation.Road;
import org.traffxml.lib.alertclocation.Segment;
import org.traffxml.traff.TraffFeed;
import org.traffxml.viewer.TraffViewer;
import org.traffxml.viewer.input.StaticSource;

import eu.datex2.schema._2._2_0.*;

public class Datex2Test {
	private static final int FLAG_CONVERT = 0x1;
	private static final int FLAG_DISSECT = 0x2;
	private static final int FLAG_DUMP = 0x4;
	private static final int FLAG_GUI = 0x10000;
	private static final int MASK_OPERATIONS = 0xFFFF;

	private static String cc = null;
	private static String ltn = null;
	
	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -convert <feed>     Convert <feed> to TraFF");
		System.out.println("  -dissect <feed>     Examine <feed> and print results");
		System.out.println("  -dump <feed>        Dump raw feed <feed>");
		System.out.println("  -gui                Launch TraFF Viewer UI (only with -convert)");
		System.out.println("  -ltdb <path>        Use AlertC location database at the given path");
		System.out.println("  -forcelt <cc>.<ltn> Force use of the specified LTN");
		System.out.println();
		System.out.println("<feed> can refer to a local file or an http/https URL.");
		System.out.println("<cc> can refer to an ISO code or an RDS code (single hex digit).");
	}

	public static void main(String[] args) {
		String input = null;
		InputStream dataInputStream = null;
		int i;
		int flags = 0;
		String dbUrl = "jdbc:hsqldb:mem:.";

		if (args.length == 0) {
			printUsage();
			System.exit(1);
		}
		for (i = 0; i < args.length; i++) {
			if ("-convert".equals(args[i])) {
				input = getParam("convert", args, ++i);
				flags |= FLAG_CONVERT;
			} else if ("-dissect".equals(args[i])) {
				input = getParam("dissect", args, ++i);
				flags |= FLAG_DISSECT;
			} else if ("-dump".equals(args[i])) {
				input = getParam("dump", args, ++i);
				flags |= FLAG_DUMP;
			} else if ("-gui".equals(args[i])) {
				flags |= FLAG_GUI;
			} else if ("-forcelt".equals(args[i])) {
				String[] ccltn = getParam("forcelt", args, ++i).split("\\.");
				if (ccltn.length > 0) {
					if (!ccltn[0].trim().isEmpty())
						cc = ccltn[0].trim();
					if ((ccltn.length > 1) && !ccltn[1].trim().isEmpty())
						ltn = ccltn[1].trim();
				}
			} else if ("-ltdb".equals(args[i])) {
				dbUrl = String.format("jdbc:hsqldb:file:%s", getParam("ltdb", args, ++i));
			} else {
				System.out.println("Unknown argument: " + args[i]);
				System.out.println();
				printUsage();
				System.exit(0);
			}
		}
		if ((input == null) || (input.isEmpty())) {
			System.err.println("Input file or URL must be specified.");
			System.exit(1);
		}

		AlertC.setDbUrl(dbUrl);

		if (input.startsWith("http://") || input.startsWith("https://")) {
			URL url;
			try {
				url = new URL(input);
				URLConnection connection = url.openConnection();
				/* if content is gzipped, unzip it on the fly */
				if ("application/gzip".equals(connection.getContentType())
						|| "application/x-gzip".equals(connection.getContentType())
						|| "gzip".equals(connection.getContentEncoding()))
					dataInputStream = new GZIPInputStream(connection.getInputStream());
				else
					dataInputStream = connection.getInputStream();
			} catch (MalformedURLException e) {
				System.err.println("Input URL is invalid. Aborting.");
				System.exit(1);
			} catch (IOException e) {
				System.err.println("Cannot read from input URL. Aborting.");
				System.exit(1);
			}
		} else {
			File inputFile = new File(input);
			if (!inputFile.exists()) {
				System.err.println("Input file does not exist. Aborting.");
				System.exit(1);
			} else if (!inputFile.isFile()) {
				System.err.println("Input file is not a file. Aborting.");
				System.exit(1);
			} else if (!inputFile.canRead()) {
				System.err.println("Input file is not readable. Aborting.");
				System.exit(1);
			}
			try {
				dataInputStream = new FileInputStream(inputFile);
			} catch (FileNotFoundException e) {
				System.err.println("Input file does not exist. Aborting.");
				System.exit(1);
			}
		}
		if (dataInputStream != null) {
			D2LogicalModel d2lm;
			switch(flags & MASK_OPERATIONS) {
			case FLAG_CONVERT:
				d2lm = Datex2Helper.getD2LogicalModel(dataInputStream);
				// assume a poll interval of 10 minutes
				try {
					TraffFeed feed = new TraffFeed(Datex2Helper.toTraff(d2lm, "test", cc, ltn, 10, null));
					if ((flags & FLAG_GUI) != 0) {
						StaticSource.readFeed(feed);
						TraffViewer.launch();
					} else
						System.out.println(feed.toXml());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case FLAG_DISSECT:
				d2lm = Datex2Helper.getD2LogicalModel(dataInputStream);
				dissectD2LogicalModel(d2lm, 0);
				break;
			case FLAG_DUMP:
				// TODO
				break;
			default:
				System.err.println("Invalid combination of options. Aborting.");
				System.exit(1);
			}
		}
	}

	private static void dissectAbnormalTraffic(AbnormalTraffic abnormalTraffic, int level) {
		if (abnormalTraffic.getAbnormalTrafficType() != null)
			System.out.println(indent(level) + "AbnormalTrafficType: " + abnormalTraffic.getAbnormalTrafficType().name());
		System.out.println(indent(level) + "NumberOfVehiclesWaiting: " + abnormalTraffic.getNumberOfVehiclesWaiting());
		System.out.println(indent(level) + "QueueLength: " + abnormalTraffic.getQueueLength());
		if (abnormalTraffic.getRelativeTrafficFlow() != null)
			System.out.println(indent(level) + "RelativeTrafficFlow: " + abnormalTraffic.getRelativeTrafficFlow().name());
		if (abnormalTraffic.getTrafficFlowCharacteristics() != null)
			System.out.println(indent(level) + "TrafficFlowCharacteristics: " + abnormalTraffic.getTrafficFlowCharacteristics().name());
		if (abnormalTraffic.getTrafficTrendType() != null)
			System.out.println(indent(level) + "TrafficTrendType: " + abnormalTraffic.getTrafficTrendType().name());
	}

	private static void dissectAccident(Accident accident, int level) {
		if (accident.getAccidentCause() != null)
			System.out.println(indent(level) + "AccidentCause: " + accident.getAccidentCause().name());
		if (accident.getAccidentType() != null) {
			System.out.println(indent(level) + "AccidentType:");
			for (AccidentTypeEnum type : accident.getAccidentType())
				System.out.println(indent(level + 1) + type.name());
		}
		if (accident.getTotalNumberOfPeopleInvolved() != null)
			System.out.println(indent(level) + "TotalNumberOfPeopleInvolved: " + accident.getTotalNumberOfPeopleInvolved());
		if (accident.getTotalNumberOfVehiclesInvolved() != null)
			System.out.println(indent(level) + "TotalNumberOfVehiclesInvolved: " + accident.getTotalNumberOfVehiclesInvolved());
		// TODO vehicleInvolved, groupOfVehiclesInvolved, groupOfPeopleInvolved
	}

	private static void dissectActivity(Activity activity, int level) {
		if ((activity.getMobilityOfActivity() != null) && (activity.getMobilityOfActivity().getMobilityType() != null))
			System.out.println(indent(level) + "MobilityOfActivity: " + activity.getMobilityOfActivity().getMobilityType().name());
		if (activity instanceof AuthorityOperation) {
			dissectAuthorityOperation((AuthorityOperation) activity, level);
		} else if (activity instanceof DisturbanceActivity) {
			System.out.println(indent(level) + "DisturbanceActivity (TODO)");
			// TODO
		} else if (activity instanceof PublicEvent) {
			dissectPublicEvent((PublicEvent) activity, level);
		}
	}

	private static void dissectAlertCArea(AlertCArea alertCArea, int level) {
		System.out.println(indent(level) + "AlertCArea (skipped)");
	}

	private static void dissectAlertCDirection(AlertCDirection alertCDirection, int level) {
		System.out.println(indent(level) + "AlertCDirection:");
		if (alertCDirection.getAlertCDirectionCoded() != null)
			System.out.println(indent(level + 1) + "AlertCDirectionCoded: " + alertCDirection.getAlertCDirectionCoded().name());
		if (alertCDirection.getAlertCDirectionNamed() != null)
			dissectMultilingualString(alertCDirection.getAlertCDirectionNamed(), "AlertCDirectionNamed", level + 1);
		if (alertCDirection.isAlertCDirectionSense() != null)
			System.out.println(indent(level + 1) + "AlertCDirectionSense: " + (alertCDirection.isAlertCDirectionSense() ? "true" : "false"));
	}

	private static void dissectAlertCExpandedLocation(org.traffxml.lib.alertclocation.AlertCLocation alertCLocation, int level) {
		System.out.println(indent(level) + alertCLocation.getClass().getSimpleName() + ":");
		if (alertCLocation.category != null)
			System.out.println(indent(level + 1) + "Class: " + alertCLocation.category.name() + " " + alertCLocation.tcd + "." + alertCLocation.stcd);
		if (alertCLocation.roadName != null)
			System.out.println(indent(level + 1) + "RoadName: " + alertCLocation.roadName.name);
		if (alertCLocation.name1 != null)
			System.out.println(indent(level + 1) + "Name1: " + alertCLocation.name1.name);
		if (alertCLocation.name2 != null)
			System.out.println(indent(level + 1) + "Name2: " + alertCLocation.name2.name);
		if (alertCLocation instanceof org.traffxml.lib.alertclocation.AlertCArea)
			// TODO
			System.out.println(indent(level + 1) + "AlertCArea (TODO)");
		else if (alertCLocation instanceof org.traffxml.lib.alertclocation.AlertCPoint)
			dissectAlertCExpandedPoint((org.traffxml.lib.alertclocation.AlertCPoint) alertCLocation, level + 1);
		else if (alertCLocation instanceof Road)
			dissectRoad((Road) alertCLocation, level + 1);
		else if (alertCLocation instanceof Segment)
			// TODO
			System.out.println(indent(level + 1) + "Segment (TODO)");
	}

	private static void dissectAlertCExpandedPoint(org.traffxml.lib.alertclocation.AlertCPoint alertCPoint, int level) {
		if (alertCPoint.junctionNumber != null)
			System.out.println(indent(level) + "JunctionNumber: " + alertCPoint.junctionNumber);
		if (alertCPoint.road != null) {
			dissectAlertCExpandedLocation(alertCPoint.road, level);
		}
		System.out.println(indent(level) + "XCoord, YCoord: " + alertCPoint.xCoord + ", " + alertCPoint.yCoord);
		dissectLatLon(alertCPoint.yCoord, alertCPoint.xCoord, level + 1);
	}

	private static void dissectAlertCLinear(AlertCLinear alertCLinear, int level) {
		System.out.println(indent(level) + "AlertCLinear: " + alertCLinear.getClass().getSimpleName());
		System.out.println(indent(level + 1) + "AlertCLocationCountryCode: " + alertCLinear.getAlertCLocationCountryCode());
		System.out.println(indent(level + 1) + "AlertCLocationTableNumber: " + alertCLinear.getAlertCLocationTableNumber());
		System.out.println(indent(level + 1) + "AlertCLocationTableVersion: " + alertCLinear.getAlertCLocationTableVersion());
		if (alertCLinear instanceof AlertCLinearByCode) {
			dissectAlertCLinearByCode((cc == null) ? alertCLinear.getAlertCLocationCountryCode() : cc,
					(ltn == null) ? alertCLinear.getAlertCLocationTableNumber() : ltn,
							(AlertCLinearByCode) alertCLinear,
							level);
		} else if (alertCLinear instanceof AlertCMethod2Linear) {
			dissectAlertCMethod2Linear((cc == null) ? alertCLinear.getAlertCLocationCountryCode() : cc,
					(ltn == null) ? alertCLinear.getAlertCLocationTableNumber() : ltn,
							(AlertCMethod2Linear) alertCLinear,
							level);
		} else if (alertCLinear instanceof AlertCMethod4Linear) {
			dissectAlertCMethod4Linear((cc == null) ? alertCLinear.getAlertCLocationCountryCode() : cc,
					(ltn == null) ? alertCLinear.getAlertCLocationTableNumber() : ltn,
							(AlertCMethod4Linear) alertCLinear,
							level);
		}
	}

	private static void dissectAlertCLinearByCode(String ccd, String tabcd, AlertCLinearByCode alertCLinearByCode, int level) {
		System.out.println(indent(level) + "AlertCLinearByCode:");
		if (alertCLinearByCode.getAlertCDirection() != null)
			dissectAlertCDirection(alertCLinearByCode.getAlertCDirection(), level + 1);
		if (alertCLinearByCode.getLocationCodeForLinearLocation() != null) {
			System.out.println(indent(level) + "LocationCodeForLinearLocation: AlertCLocation");
			dissectAlertCLocation(ccd, tabcd, alertCLinearByCode.getLocationCodeForLinearLocation(), level + 1);
		}
	}

	private static void dissectAlertCLocation(String country, String tabcd, AlertCLocation alertCLocation, int level) {
		if (alertCLocation.getAlertCLocationName() != null)
			dissectMultilingualString(alertCLocation.getAlertCLocationName(), "AlertCLocationName", level);
		if (alertCLocation.getSpecificLocation() != null) {
			System.out.println(indent(level) + "SpecificLocation: " + alertCLocation.getSpecificLocation());
			int tabcdInt = -1;
			try {
				tabcdInt = Integer.valueOf(tabcd);
			} catch (NumberFormatException e) {
				/*
				System.err.println("TABCD is not a valid integer: " + tabcd);
				e.printStackTrace();
				*/
			}
			if (tabcdInt != -1) {
				org.traffxml.lib.alertclocation.AlertCLocation expanded = null;
				if (country.matches("[0-9a-fA-F]")) {
					/*
					 * ccd is a valid CCD (single hex digit).
					 * Within Europe, CCD/TABCD uniquely identifies the country. Globally, collisions are possible:
					 * For example, 3/1 is used by both Andorra and Victoria (Australia). A/1 is used by both Austria
					 * and Singapore. The US and Canada use CCDs from 1 to E with TABCDs from 1 to 36, which collides
					 * with numerous other countries.
					 * Such collisions are not an issue as long as both the input we process and the location table
					 * are constrained to an area that is free from collisions. Otherwise we need to use the ISO code
					 * or CID, both of which are unique.
					 */
					expanded = AlertC.getLocation(country, tabcdInt, alertCLocation.getSpecificLocation().intValue());
				} else if (country.matches("[a-zA-Z]{2}")) {
					/* ccd is the ISO code, not the CCD */
					Integer cid = Datex2Helper.getCidFromIso(country);
					if (cid != null)
						/* resolve location with CID */
						expanded = AlertC.getLocation(cid, tabcdInt, alertCLocation.getSpecificLocation().intValue());
				}
				if (expanded != null)
					dissectAlertCExpandedLocation(expanded, level + 1);
			}
		}
	}

	private static void dissectAlertCMethod2Linear(String ccd, String tabcd, AlertCMethod2Linear alertCMethod2Linear, int level) {
		if (alertCMethod2Linear.getAlertCDirection() != null)
			dissectAlertCDirection(alertCMethod2Linear.getAlertCDirection(), level + 1);
		if (alertCMethod2Linear.getAlertCMethod2PrimaryPointLocation() != null)
			dissectAlertCMethod2PrimaryPointLocation(ccd, tabcd, alertCMethod2Linear.getAlertCMethod2PrimaryPointLocation(), level + 1);
		if (alertCMethod2Linear.getAlertCMethod2SecondaryPointLocation() != null)
			dissectAlertCMethod2SecondaryPointLocation(ccd, tabcd, alertCMethod2Linear.getAlertCMethod2SecondaryPointLocation(), level + 1);
	}

	private static void dissectAlertCMethod2Point(String ccd, String tabcd, AlertCMethod2Point alertCMethod2Point, int level) {
		if (alertCMethod2Point.getAlertCDirection() != null)
			dissectAlertCDirection(alertCMethod2Point.getAlertCDirection(), level + 1);
		if (alertCMethod2Point.getAlertCMethod2PrimaryPointLocation() != null)
			dissectAlertCMethod2PrimaryPointLocation(ccd, tabcd, alertCMethod2Point.getAlertCMethod2PrimaryPointLocation(), level + 1);
	}

	private static void dissectAlertCMethod2PrimaryPointLocation(
			String ccd, String tabcd, AlertCMethod2PrimaryPointLocation alertCMethod2PointLocation, int level) {
		System.out.println(indent(level) + alertCMethod2PointLocation.getClass().getSimpleName() + ":");
		if (alertCMethod2PointLocation.getAlertCLocation() != null)
			dissectAlertCLocation(ccd, tabcd, alertCMethod2PointLocation.getAlertCLocation(), level + 1);
	}

	private static void dissectAlertCMethod2SecondaryPointLocation(
			String ccd, String tabcd, AlertCMethod2SecondaryPointLocation alertCMethod4PointLocation, int level) {
		System.out.println(indent(level) + alertCMethod4PointLocation.getClass().getSimpleName() + ":");
		if (alertCMethod4PointLocation.getAlertCLocation() != null)
			dissectAlertCLocation(ccd, tabcd, alertCMethod4PointLocation.getAlertCLocation(), level + 1);
	}

	private static void dissectAlertCMethod4Linear(String ccd, String tabcd, AlertCMethod4Linear alertCMethod4Linear, int level) {
		if (alertCMethod4Linear.getAlertCDirection() != null)
			dissectAlertCDirection(alertCMethod4Linear.getAlertCDirection(), level + 1);
		if (alertCMethod4Linear.getAlertCMethod4PrimaryPointLocation() != null)
			dissectAlertCMethod4PrimaryPointLocation(ccd, tabcd, alertCMethod4Linear.getAlertCMethod4PrimaryPointLocation(), level + 1);
		if (alertCMethod4Linear.getAlertCMethod4SecondaryPointLocation() != null)
			dissectAlertCMethod4SecondaryPointLocation(ccd, tabcd, alertCMethod4Linear.getAlertCMethod4SecondaryPointLocation(), level + 1);
	}

	private static void dissectAlertCMethod4Point(String ccd, String tabcd, AlertCMethod4Point alertCMethod4Point, int level) {
		if (alertCMethod4Point.getAlertCDirection() != null)
			dissectAlertCDirection(alertCMethod4Point.getAlertCDirection(), level + 1);
		if (alertCMethod4Point.getAlertCMethod4PrimaryPointLocation() != null)
			dissectAlertCMethod4PrimaryPointLocation(ccd, tabcd, alertCMethod4Point.getAlertCMethod4PrimaryPointLocation(), level + 1);
	}

	private static void dissectAlertCMethod4PrimaryPointLocation(
			String ccd, String tabcd, AlertCMethod4PrimaryPointLocation alertCMethod4PointLocation, int level) {
		System.out.println(indent(level) + alertCMethod4PointLocation.getClass().getSimpleName() + ":");
		if (alertCMethod4PointLocation.getAlertCLocation() != null)
			dissectAlertCLocation(ccd, tabcd, alertCMethod4PointLocation.getAlertCLocation(), level + 1);
		if ((alertCMethod4PointLocation.getOffsetDistance() != null) && (alertCMethod4PointLocation.getOffsetDistance().getOffsetDistance() != null))
			System.out.println(indent(level + 1) + "OffsetDistance: " + alertCMethod4PointLocation.getOffsetDistance().getOffsetDistance());
	}

	private static void dissectAlertCMethod4SecondaryPointLocation(
			String ccd, String tabcd, AlertCMethod4SecondaryPointLocation alertCMethod4PointLocation, int level) {
		System.out.println(indent(level) + alertCMethod4PointLocation.getClass().getSimpleName() + ":");
		if (alertCMethod4PointLocation.getAlertCLocation() != null)
			dissectAlertCLocation(ccd, tabcd, alertCMethod4PointLocation.getAlertCLocation(), level + 1);
		if ((alertCMethod4PointLocation.getOffsetDistance() != null) && (alertCMethod4PointLocation.getOffsetDistance().getOffsetDistance() != null))
			System.out.println(indent(level + 1) + "OffsetDistance: " + alertCMethod4PointLocation.getOffsetDistance().getOffsetDistance());
	}

	private static void dissectAlertCPoint(AlertCPoint alertCPoint, int level) {
		System.out.println(indent(level) + "AlertCPoint: " + alertCPoint.getClass().getSimpleName());
		System.out.println(indent(level + 1) + "AlertCLocationCountryCode: " + alertCPoint.getAlertCLocationCountryCode());
		System.out.println(indent(level + 1) + "AlertCLocationTableNumber: " + alertCPoint.getAlertCLocationTableNumber());
		System.out.println(indent(level + 1) + "AlertCLocationTableVersion: " + alertCPoint.getAlertCLocationTableVersion());
		if (alertCPoint instanceof AlertCMethod2Point) {
			dissectAlertCMethod2Point((cc == null) ? alertCPoint.getAlertCLocationCountryCode() : cc,
					(ltn == null) ? alertCPoint.getAlertCLocationTableNumber() : ltn,
							(AlertCMethod2Point) alertCPoint,
							level + 1);
		} else if (alertCPoint instanceof AlertCMethod4Point) {
			dissectAlertCMethod4Point((cc == null) ? alertCPoint.getAlertCLocationCountryCode() : cc,
					(ltn == null) ? alertCPoint.getAlertCLocationTableNumber() : ltn,
							(AlertCMethod4Point) alertCPoint,
							level + 1);
		}
	}

	private static void dissectAnimalPresenceObstruction(AnimalPresenceObstruction animalPresenceObstruction, int level) {
		if (animalPresenceObstruction.isAlive() != null)
			System.out.println(indent(level) + "Alive: " + (animalPresenceObstruction.isAlive() ? "true" : "false"));
		if (animalPresenceObstruction.getAnimalPresenceType() != null)
			System.out.println(indent(level) + "AnimalPresenceType: " + animalPresenceObstruction.getAnimalPresenceType().name());
	}

	private static void dissectArea(Area area, int level) {
		if (area.getAlertCArea() != null)
			dissectAlertCArea(area.getAlertCArea(), level);
		if (area.getTpegAreaLocation() != null)
			dissectTpegAreaLocation(area.getTpegAreaLocation(), level);
	}

	private static void dissectAreaDestination(AreaDestination areaDestination, int level) {
		if (areaDestination.getArea() != null) {
			System.out.println(indent(level) + "Area:");
			dissectArea(areaDestination.getArea(), level + 1);
		}
	}

	private static void dissectAuthorityOperation(AuthorityOperation authorityOperation, int level) {
		if (authorityOperation.getAuthorityOperationType() != null)
			System.out.println(indent(level) + "AuthorityOperationType: " + authorityOperation.getAuthorityOperationType().name());
	}

	private static void dissectCause(Cause cause, int level) {
		System.out.println(indent(level) + "Cause: " + cause.getClass().getSimpleName());
		if (cause instanceof ManagedCause)
			dissectManagedCause((ManagedCause) cause, level + 1);
		else if (cause instanceof NonManagedCause)
			dissectNonManagedCause((NonManagedCause) cause, level + 1);
	}

	private static void dissectConditions(Conditions conditions, int level) {
		if (conditions.getDrivingConditionType() != null)
			System.out.println(indent(level) + "DrivingConditionType: " + conditions.getDrivingConditionType().name());
		if (conditions instanceof PoorEnvironmentConditions) {
			dissectPoorEnvironmentConditions((PoorEnvironmentConditions) conditions, level);
		} else if (conditions instanceof RoadConditions) {
			dissectRoadConditions((RoadConditions) conditions, level);
		}
	}

	private static void dissectConstructionWorks(ConstructionWorks constructionWorks, int level) {
		if (constructionWorks.getConstructionWorkType() != null)
			System.out.println(indent(level) + "ConstructionWorkType: " + constructionWorks.getConstructionWorkType().name());
	}

	private static void dissectD2LogicalModel(D2LogicalModel d2lm, int level) {
		if (d2lm != null) {
			System.out.println(indent(level) + "D2LogicalModel");
			if (d2lm.getPayloadPublication() != null)
				dissectPayloadPublication(d2lm.getPayloadPublication(), level + 1);
		} else
			System.out.println(indent(level) + "D2LogicalModel: null");
	}

	private static void dissectDayWeekMonth(List<DayWeekMonth> recurringDayWeekMonthPeriod, String name, int level) {
		System.out.println(indent(level) + name + ": (" + recurringDayWeekMonthPeriod.size() + ")");
		for (DayWeekMonth dayWeekMonth : recurringDayWeekMonthPeriod)
			dissectDayWeekMonth(dayWeekMonth, level + 1);
	}

	private static void dissectDayWeekMonth(DayWeekMonth dayWeekMonth, int level) {
		System.out.println(indent(level) + "DayWeekMonth:");
		if (dayWeekMonth.getApplicableDay() != null) {
			System.out.println(indent(level + 1) + "ApplicableDay:");
			for (DayEnum day : dayWeekMonth.getApplicableDay())
				System.out.println(indent(level + 2) + day.name());
		}
		if (dayWeekMonth.getApplicableWeek() != null) {
			System.out.println(indent(level + 1) + "ApplicableWeek:");
			for (WeekOfMonthEnum week : dayWeekMonth.getApplicableWeek())
				System.out.println(indent(level + 2) + week.name());
		}
		if (dayWeekMonth.getApplicableMonth() != null) {
			System.out.println(indent(level + 1) + "ApplicableMonth:");
			for (MonthOfYearEnum month : dayWeekMonth.getApplicableMonth())
				System.out.println(indent(level + 2) + month.name());
		}
	}

	private static void dissectDelays(Delays delays, int level) {
		System.out.println(indent(level) + "Delays:");
		if (delays.getDelayBand() != null)
			System.out.println(indent(level + 1) + "DelayBand: " + delays.getDelayBand().name());
		if (delays.getDelaysType() != null)
			System.out.println(indent(level + 1) + "DelaysType: " + delays.getDelaysType().name());
		System.out.println(indent(level + 1) + "DelayTimeValue: " + delays.getDelayTimeValue());
	}

	private static void dissectDestination(Destination destination, int level) {
		System.out.println(indent(level) + "Destination: " + destination.getClass().getSimpleName());
		if (destination instanceof AreaDestination)
			dissectAreaDestination((AreaDestination) destination, level + 1);
		else if (destination instanceof PointDestination)
			dissectPointDestination((PointDestination) destination, level + 1);
	}

	private static void dissectDistanceAlongLinearElement(DistanceAlongLinearElement distanceAlongLinearElement, String name,
			int level) {
		System.out.println(indent(level) + name + ": " + distanceAlongLinearElement.getClass().getSimpleName());
		// nothing to do here, apart from extensions
		if (distanceAlongLinearElement instanceof DistanceFromLinearElementReferent) {
			dissectDistanceFromLinearElementReferent((DistanceFromLinearElementReferent) distanceAlongLinearElement, level + 1);
		} else if (distanceAlongLinearElement instanceof DistanceFromLinearElementStart) {
			dissectDistanceFromLinearElementStart((DistanceFromLinearElementStart) distanceAlongLinearElement, level + 1);
		} else if (distanceAlongLinearElement instanceof PercentageDistanceAlongLinearElement) {
			dissectPercentageDistanceAlongLinearElement((PercentageDistanceAlongLinearElement) distanceAlongLinearElement, level + 1);
		}
	}

	private static void dissectDistanceFromLinearElementReferent(
			DistanceFromLinearElementReferent distanceFromLinearElementReferent, int level) {
		System.out.println(indent(level) + "DistanceAlong: " + distanceFromLinearElementReferent.getDistanceAlong());
		if (distanceFromLinearElementReferent.getFromReferent() != null)
			dissectReferent(distanceFromLinearElementReferent.getFromReferent(), "FromReferent", level);
		if (distanceFromLinearElementReferent.getTowardsReferent() != null)
			dissectReferent(distanceFromLinearElementReferent.getTowardsReferent(), "TowardsReferent", level);
	}

	private static void dissectDistanceFromLinearElementStart(DistanceFromLinearElementStart distanceFromLinearElementStart,
			int level) {
		System.out.println(indent(level) + "DistanceAlong: " + distanceFromLinearElementStart.getDistanceAlong());
	}

	// TODO use this for all enums
	private static void dissectEnum(Enum en, String name, int level) {
		System.out.println(indent(level) + name + ": " + ((en != null) ? en.name() : "(null)"));
	}

	private static void dissectEnvironmentalObstruction(EnvironmentalObstruction environmentalObstruction, int level) {
		if (environmentalObstruction.getDepth() != null)
			System.out.println(indent(level) + "Depth: " + environmentalObstruction.getDepth());
		if (environmentalObstruction.getEnvironmentalObstructionType() != null)
			System.out.println(indent(level) + "EnvironmentalObstructionType: " + environmentalObstruction.getEnvironmentalObstructionType().name());
	}

	private static void dissectExtension(ExtensionType extension, String name, int level) {
		System.out.println(indent(level) + name + ":");
		for (Object object : extension.getAny())
			System.out.println(indent(level + 1) + object.getClass().getCanonicalName() + ": " + object.toString());
		// TODO dissect com.sun.org.apache.xerces.internal.dom.ElementNSImpl
	}

	private static void dissectGeneralInstructionOrMessageToRoadUsers(GeneralInstructionOrMessageToRoadUsers generalInstructionOrMessageToRoadUsers, int level) {
		if (generalInstructionOrMessageToRoadUsers.getGeneralInstructionToRoadUsersType() != null)
			System.out.println(indent(level) + "GeneralInstructionToRoadUsersType: " + generalInstructionOrMessageToRoadUsers.getGeneralInstructionToRoadUsersType().name());
		// skip generalMessageToRoadUsers (prose text)
	}

	private static void dissectGeneralNetworkManagement(GeneralNetworkManagement generalNetworkManagement, int level) {
		if (generalNetworkManagement.getGeneralNetworkManagementType() != null)
			System.out.println(indent(level) + "GeneralNetworkManagementType: " + generalNetworkManagement.getGeneralNetworkManagementType().name());
		if (generalNetworkManagement.getTrafficManuallyDirectedBy() != null)
			System.out.println(indent(level) + "TrafficManuallyDirectedBy: " + generalNetworkManagement.getTrafficManuallyDirectedBy().name());
	}

	private static void dissectGeneralObstruction(GeneralObstruction generalObstruction, int level) {
		if (generalObstruction.getObstructionType() != null) {
			System.out.println(indent(level) + "ObstructionType:");
			for (ObstructionTypeEnum type : generalObstruction.getObstructionType())
				System.out.println(indent(level + 1) + type.name());
		}
		// skip groupOfPeopleInvolved
	}

	private static void dissectGroupOfLocations(GroupOfLocations groupOfLocations, int level) {
		System.out.println(indent(level) + "GroupOfLocations: " + groupOfLocations.getClass().getSimpleName());
		if (groupOfLocations instanceof Itinerary) {
			dissectItinerary((Itinerary) groupOfLocations, level + 1);
		} else if (groupOfLocations instanceof Location) {
			dissectLocation((Location) groupOfLocations, level + 1);
		} else if (groupOfLocations instanceof NonOrderedLocations) {
			dissectNonOrderedLocations((NonOrderedLocations) groupOfLocations, level + 1);
		}
		if (groupOfLocations.getGroupOfLocationsExtension() != null)
			dissectExtension(groupOfLocations.getGroupOfLocationsExtension(), "GroupOfLocationsExtension", level);
	}

	private static void dissectImpact(Impact impact, int level) {
		System.out.println(indent(level) + "Impact:");
		System.out.println(indent(level + 1) + "CapacityRemaining: " + impact.getCapacityRemaining());
		System.out.println(indent(level + 1) + "OriginalNumberOfLanes: " + impact.getOriginalNumberOfLanes());
		System.out.println(indent(level + 1) + "NumberOfLanesRestricted: " + impact.getNumberOfLanesRestricted());
		System.out.println(indent(level + 1) + "NumberOfOperationalLanes: " + impact.getNumberOfOperationalLanes());
		System.out.println(indent(level + 1) + "ResidualRoadWidth: " + impact.getResidualRoadWidth());
		dissectEnum(impact.getTrafficConstrictionType(), "TrafficConstrictionType", level + 1);
		if (impact.getDelays() != null)
			dissectDelays(impact.getDelays(), level + 1);
	}

	private static void dissectItinerary(Itinerary itinerary, int level) {
		if (itinerary.getRouteDestination() != null) {
			System.out.println(indent(level) + "RouteDestination:");
			for (Destination destination : itinerary.getRouteDestination()) {
				dissectDestination(destination, level + 1);
			}
		}
		if (itinerary instanceof ItineraryByIndexedLocations) {
			dissectItineraryByIndexedLocations((ItineraryByIndexedLocations) itinerary, level);
		} else if (itinerary instanceof ItineraryByReference) {
			dissectItineraryByReference((ItineraryByReference) itinerary, level);
		}
	}

	private static void dissectItineraryByIndexedLocations(ItineraryByIndexedLocations itineraryByIndexedLocations, int level) {
		System.out.println(indent(level) + "ItineraryByIndexedLocations:");
		if (itineraryByIndexedLocations.getLocationContainedInItinerary() != null) {
			System.out.println(indent(level + 1) + "LocationContainedInItinerary: (" + itineraryByIndexedLocations.getLocationContainedInItinerary().size() + ")");
			for (LocationContainedInItinerary locationContainedInItinerary : itineraryByIndexedLocations.getLocationContainedInItinerary())
				dissectLocationContainedInItinerary(locationContainedInItinerary, level + 2);
		}
	}

	private static void dissectItineraryByReference(ItineraryByReference itineraryByReference, int level) {
		System.out.println(indent(level) + "ItineraryByReference (TODO)");
		// TODO
	}

	private static void dissectLatLon(float lat, float lon, int level) {
		System.out.println(indent(level) + "https://www.openstreetmap.org/?mlat="
				+ lat + "&mlon=" + lon + "#map=14/" + lat + "/" + lon);
	}

	private static void dissectLinear(Linear linear, int level) {
		if (linear.getTpegLinearLocation() != null)
			dissectTpegLinearLocation(linear.getTpegLinearLocation(), level);
		if (linear.getAlertCLinear() != null)
			dissectAlertCLinear(linear.getAlertCLinear(), level);
		if (linear.getLinearWithinLinearElement() != null)
			dissectLinearWithinLinearElement(linear.getLinearWithinLinearElement(), level);
		if (linear.getLinearExtension() != null)
			dissectLinearExtension(linear.getLinearExtension(), "LinearExtension", level);
	}

	private static void dissectLinearElement(LinearElement linearElement, int level) {
		System.out.println(indent(level) + "LinearElement: " + linearElement.getClass().getSimpleName());
		if (linearElement.getRoadName() != null)
			dissectMultilingualString(linearElement.getRoadName(), "RoadName", level + 1);
		System.out.println(indent(level + 1) + "RoadNumber: " + linearElement.getRoadNumber());
		System.out.println(indent(level + 1) + "LinearElementReferenceModel: " + linearElement.getLinearElementReferenceModel());
		System.out.println(indent(level + 1) + "LinearElementReferenceModelVersion: " + linearElement.getLinearElementReferenceModelVersion());
		if (linearElement.getLinearElementNature() != null)
			System.out.println(indent(level + 1) + "LinearElementNature: " + linearElement.getLinearElementNature().name());
		if (linearElement instanceof LinearElementByCode) {
			dissectLinearElementByCode((LinearElementByCode) linearElement, level + 1);
		} else if (linearElement instanceof LinearElementByPoints) {
			dissectLinearElementByPoints((LinearElementByPoints) linearElement, level + 1);
		}
	}

	private static void dissectLinearElementByCode(LinearElementByCode linearElementByCode, int level) {
		System.out.println(indent(level) + "LinearElementIdentifier: " + linearElementByCode.getLinearElementIdentifier());
	}

	private static void dissectLinearElementByPoints(LinearElementByPoints linearElementByPoints, int level) {
		if (linearElementByPoints.getStartPointOfLinearElement() != null)
			dissectReferent(linearElementByPoints.getStartPointOfLinearElement(), "StartPointOfLinearElement", level);
		if (linearElementByPoints.getIntermediatePointOnLinearElement() != null) {
			System.out.println(indent(level) + "IntermediatePointOnLinearElement: " + linearElementByPoints.getIntermediatePointOnLinearElement().size());
			for (IntermediatePointOnLinearElement point : linearElementByPoints.getIntermediatePointOnLinearElement())
				if (point.getReferent() != null)
					dissectReferent(point.getReferent(), String.valueOf(point.getIndex()), level + 1);
		}
		if (linearElementByPoints.getEndPointOfLinearElement() != null)
			dissectReferent(linearElementByPoints.getEndPointOfLinearElement(), "EndPointOfLinearElement", level);
	}

	private static void dissectLinearExtension(LinearExtensionType extension, String name, int level) {
		System.out.println(indent(level) + name + ":");
		if (extension.getOpenlrExtendedLinear() != null)
			dissectOpenlrExtendedLinear(extension.getOpenlrExtendedLinear(), level + 1);
		/*
		for (Object object : extension.getAny())
			System.out.println(indent(level + 1) + object.getClass().getCanonicalName() + ": " + object.toString());
		// TODO dissect com.sun.org.apache.xerces.internal.dom.ElementNSImpl
		 */
	}

	private static void dissectLinearWithinLinearElement(LinearWithinLinearElement linearWithinLinearElement, int level) {
		System.out.println(indent(level) + "LinearWithinLinearElement:");
		if (linearWithinLinearElement.getAdministrativeAreaOfLinearSection() != null)
			dissectMultilingualString(linearWithinLinearElement.getAdministrativeAreaOfLinearSection(), "AdministrativeAreaOfLinearSection", level + 1);
		if (linearWithinLinearElement.getDirectionBoundOnLinearSection() != null)
			System.out.println(indent(level + 1) + "DirectionBoundOnLinearSection: " + linearWithinLinearElement.getDirectionBoundOnLinearSection().name());
		if (linearWithinLinearElement.getDirectionRelativeOnLinearSection() != null)
			System.out.println(indent(level + 1) + "DirectionRelativeOnLinearSection: " + linearWithinLinearElement.getDirectionRelativeOnLinearSection().name());
		if (linearWithinLinearElement.getHeightGradeOfLinearSection() != null)
			System.out.println(indent(level + 1) + "HeightGradeOfLinearSection: " + linearWithinLinearElement.getHeightGradeOfLinearSection().name());
		if (linearWithinLinearElement.getLinearElement() != null)
			dissectLinearElement(linearWithinLinearElement.getLinearElement(), level + 1);
		if (linearWithinLinearElement.getFromPoint() != null)
			dissectDistanceAlongLinearElement(linearWithinLinearElement.getFromPoint(), "FromPoint", level + 1);
		if (linearWithinLinearElement.getToPoint() != null)
			dissectDistanceAlongLinearElement(linearWithinLinearElement.getToPoint(), "ToPoint", level + 1);
	}

	private static void dissectLocation(Location location, int level) {
		System.out.println(indent(level) + "Location:");
		if (location.getLocationForDisplay() != null)
			System.out.println(indent(level + 1) + "LocationForDisplay: "
					+ location.getLocationForDisplay().getLatitude() + " " + location.getLocationForDisplay().getLongitude());
		if (location.getExternalReferencing() != null) {
			System.out.println(indent(level + 1) + "ExternalReferencing: (" + location.getExternalReferencing().size() + ")");
			for (ExternalReferencing externalReferencing : location.getExternalReferencing()) {
				System.out.println(indent(level + 2) + "ExternalReferencingSystem: " + externalReferencing.getExternalReferencingSystem());
				System.out.println(indent(level + 2) + "ExternalLocationCode: " + externalReferencing.getExternalLocationCode());
			}
		}
		if (location instanceof Area) {
			dissectArea((Area) location, level);
		} else if (location instanceof LocationByReference) {
			dissectLocationByReference((LocationByReference) location, level);
		} else if (location instanceof NetworkLocation) {
			dissectNetworkLocation((NetworkLocation) location, level);
		}
	}

	private static void dissectLocationByReference(LocationByReference locationByReference, int level) {
		System.out.println(indent(level) + "LocationByReference (TODO)");
		// TODO
	}

	private static void dissectLocationContainedInItinerary(LocationContainedInItinerary locationContainedInItinerary,
			int level) {
		System.out.println(indent(level) + "Index: " + locationContainedInItinerary.getIndex());
		if (locationContainedInItinerary.getLocation() != null)
			dissectLocation(locationContainedInItinerary.getLocation(), level);
	}

	private static void dissectManagedCause(ManagedCause managedCause, int level) {
		System.out.println(indent(level) + "(TODO)");
	}

	private static void dissectMaintenanceWorks(MaintenanceWorks maintenanceWorks, int level) {
		if (maintenanceWorks.getRoadMaintenanceType() != null) {
			System.out.println(indent(level) + "RoadMaintenanceType:");
			for (RoadMaintenanceTypeEnum type : maintenanceWorks.getRoadMaintenanceType())
				System.out.println(indent(level + 1) + type.name());
		}
	}

	private static void dissectMultilingualString(MultilingualString multilingualString, String name, int level) {
		System.out.println(indent(level) + name + ": MultilingualString");
		if (multilingualString.getValues() == null)
			return;
		if (multilingualString.getValues().getValue() == null)
			return;
		for (MultilingualStringValue value : multilingualString.getValues().getValue())
			System.out.println(indent(level + 1) + value.getLang() + ": " + value.getValue());
	}

	private static void dissectNetworkLocation(NetworkLocation networkLocation, int level) {
		if (networkLocation.getDestination() != null)
			dissectDestination(networkLocation.getDestination(), level);
		if (networkLocation.getSupplementaryPositionalDescription() != null)
			dissectSupplementaryPositionalDescription(networkLocation.getSupplementaryPositionalDescription(), level);
		if (networkLocation instanceof Linear) {
			dissectLinear((Linear) networkLocation, level);
		} else if (networkLocation instanceof Point) {
			dissectPoint((Point) networkLocation, level);
		}
	}

	private static void dissectNetworkManagement(NetworkManagement networkManagement, int level) {
		if (networkManagement.getComplianceOption() != null)
			System.out.println(indent(level) + "OperatorActionStatus: " + networkManagement.getComplianceOption().name());
		if (networkManagement.getApplicableForTrafficDirection() != null) {
			System.out.println(indent(level) + "ApplicableForTrafficDirection:");
			for (DirectionEnum direction : networkManagement.getApplicableForTrafficDirection())
				System.out.println(indent(level + 1) + direction.name());
		}
		if (networkManagement.getApplicableForTrafficType() != null) {
			System.out.println(indent(level) + "ApplicableForTrafficType:");
			for (TrafficTypeEnum type : networkManagement.getApplicableForTrafficType())
				System.out.println(indent(level + 1) + type.name());
		}
		if (networkManagement.getPlacesAtWhichApplicable() != null) {
			System.out.println(indent(level) + "PlacesAtWhichApplicable:");
			for (PlacesEnum place : networkManagement.getPlacesAtWhichApplicable())
				System.out.println(indent(level + 1) + place.name());
		}
		if (networkManagement.isAutomaticallyInitiated() != null)
			System.out.println(indent(level) + "AutomaticallyInitiated: " + (networkManagement.isAutomaticallyInitiated() ? "true" : "false"));
		if (networkManagement.getForVehiclesWithCharacteristicsOf() != null) {
			System.out.println(indent(level) + "ForVehiclesWithCharacteristicsOf: (" + networkManagement.getForVehiclesWithCharacteristicsOf().size() + ")");
			/* TODO
			for (VehicleCharacteristics characteristics : situationRecord.getForVehiclesWithCharacteristicsOf())
				System.out.println(indent(level + 1) + ...);
			 */
		}
		if (networkManagement instanceof GeneralInstructionOrMessageToRoadUsers) {
			dissectGeneralInstructionOrMessageToRoadUsers((GeneralInstructionOrMessageToRoadUsers) networkManagement, level);
		} else if (networkManagement instanceof GeneralNetworkManagement) {
			dissectGeneralNetworkManagement((GeneralNetworkManagement) networkManagement, level);
		} else if (networkManagement instanceof ReroutingManagement) {
			dissectReroutingManagement((ReroutingManagement) networkManagement, level);
		} else if (networkManagement instanceof RoadOrCarriagewayOrLaneManagement) {
			dissectRoadOrCarriagewayOrLaneManagement((RoadOrCarriagewayOrLaneManagement)networkManagement, level);
		} else if (networkManagement instanceof SpeedManagement) {
			dissectSpeedManagement((SpeedManagement) networkManagement, level);
		} else if (networkManagement instanceof WinterDrivingManagement) {
			System.out.println(indent(level) + "WinterDrivingManagement (TODO)");
			// TODO
		}
	}

	private static void dissectNonManagedCause(NonManagedCause nonManagedCause, int level) {
		if (nonManagedCause.getCauseDescription() != null)
			dissectMultilingualString(nonManagedCause.getCauseDescription(), "CauseDescription", level);
		dissectEnum(nonManagedCause.getCauseType(), "CauseType", level);
	}

	private static void dissectNonOrderedLocationGroupByList(NonOrderedLocationGroupByList nonOrderedLocationGroupByList, int level) {
		if (nonOrderedLocationGroupByList.getLocationContainedInGroup() != null) {
			System.out.println(indent(level) + "LocationContainedInGroup: (" + nonOrderedLocationGroupByList.getLocationContainedInGroup().size() + ")");
			for (Location location : nonOrderedLocationGroupByList.getLocationContainedInGroup())
				dissectLocation(location, level + 1);
		}
	}

	private static void dissectNonOrderedLocationGroupByReference(
			NonOrderedLocationGroupByReference nonOrderedLocationGroupByReference, int level) {
		System.out.println(indent(level) + "dissectNonOrderedLocationGroupByReference (TODO)");
		// TODO
	}

	private static void dissectNonOrderedLocations(NonOrderedLocations nonOrderedLocations, int level) {
		// nothing to do here, apart from extensions
		if (nonOrderedLocations instanceof NonOrderedLocationGroupByList) {
			dissectNonOrderedLocationGroupByList((NonOrderedLocationGroupByList) nonOrderedLocations, level);
		} else if (nonOrderedLocations instanceof NonOrderedLocationGroupByReference) {
			dissectNonOrderedLocationGroupByReference((NonOrderedLocationGroupByReference) nonOrderedLocations, level);
		}
	}

	private static void dissectNonRoadEventInformation(NonRoadEventInformation nonRoadEventInformation, int level) {
		System.out.println(indent(level) + "NonRoadEventInformation (skipped)");
		// nothing to do here, apart from extensions
		if (nonRoadEventInformation instanceof CarParks) {
			// TODO
		} else if (nonRoadEventInformation instanceof RoadOperatorServiceDisruption) {
			// TODO
		} else if (nonRoadEventInformation instanceof RoadsideServiceDisruption) {
			// TODO
		} else if (nonRoadEventInformation instanceof TransitInformation) {
			// TODO
		}
	}

	private static void dissectObstruction(Obstruction obstruction, int level) {
		System.out.println(indent(level) + "NumberOfObstructions: " + obstruction.getNumberOfObstructions());
		if ((obstruction.getMobilityOfObstruction() != null) && obstruction.getMobilityOfObstruction().getMobilityType() != null)
			System.out.println(indent(level) + "MobilityOfObstruction: " + obstruction.getMobilityOfObstruction().getMobilityType().name());
		if (obstruction instanceof AnimalPresenceObstruction) {
			dissectAnimalPresenceObstruction((AnimalPresenceObstruction) obstruction, level);
		} else if (obstruction instanceof EnvironmentalObstruction) {
			dissectEnvironmentalObstruction((EnvironmentalObstruction) obstruction, level);
		} else if (obstruction instanceof GeneralObstruction) {
			dissectGeneralObstruction((GeneralObstruction) obstruction, level);
		} else if (obstruction instanceof InfrastructureDamageObstruction) {
			System.out.println(indent(level) + "InfrastructureDamageObstruction (TODO)");
			// TODO
		} else if (obstruction instanceof VehicleObstruction) {
			dissectVehicleObstruction((VehicleObstruction) obstruction, level);
		}
	}

	private static void dissectOperatorAction(OperatorAction operatorAction, int level) {
		if (operatorAction.getActionOrigin() != null)
			System.out.println(indent(level) + "ActionOrigin: " + operatorAction.getActionOrigin().name());
		System.out.println(indent(level) + "ActionPlanIdentifier: " + operatorAction.getActionPlanIdentifier());
		if (operatorAction.getOperatorActionStatus() != null)
			System.out.println(indent(level) + "OperatorActionStatus: " + operatorAction.getOperatorActionStatus().name());
		if (operatorAction instanceof NetworkManagement) {
			dissectNetworkManagement((NetworkManagement)operatorAction, level);
		} else if (operatorAction instanceof RoadsideAssistance) {
			System.out.println(indent(level) + "RoadsideAssistance (TODO)");
			// TODO
		} else if (operatorAction instanceof Roadworks) {
			dissectRoadworks((Roadworks) operatorAction, level);
		} else if (operatorAction instanceof SignSetting) {
			System.out.println(indent(level) + "SignSetting (TODO)");
			// TODO
		}
	}

	private static void dissectOpenlrBaseLocationReferencePoint(
			OpenlrBaseLocationReferencePoint openlrBaseLocationReferencePoint, String name, int level) {
		System.out.println(indent(level) + name + ":");
		if (openlrBaseLocationReferencePoint.getOpenlrCoordinate() != null)
			dissectPointCoordinates(openlrBaseLocationReferencePoint.getOpenlrCoordinate(), "OpenlrCoordinate", level + 1);
		if (openlrBaseLocationReferencePoint.getOpenlrLineAttributes() != null)
			dissectOpenlrLineAttributes(openlrBaseLocationReferencePoint.getOpenlrLineAttributes(), level + 1);
		if (openlrBaseLocationReferencePoint instanceof OpenlrLocationReferencePoint) {
			dissectOpenlrLocationReferencePoint((OpenlrLocationReferencePoint) openlrBaseLocationReferencePoint, level + 1);
		}
		// nothing to do for OpenlrLastLocationReferencePoint (extension only)
	}

	private static void dissectOpenlrExtendedLinear(OpenlrExtendedLinear openlrExtendedLinear, int level) {
		System.out.println(indent(level) + "OpenlrExtendedLinear:");
		if (openlrExtendedLinear.getFirstDirection() != null)
			dissectOpenlrLineLocationReference(openlrExtendedLinear.getFirstDirection(), "FirstDirection", level + 1);
		if (openlrExtendedLinear.getOppositeDirection() != null)
			dissectOpenlrLineLocationReference(openlrExtendedLinear.getOppositeDirection(), "OppositeDirection", level + 1);
	}

	private static void dissectOpenlrLineAttributes(OpenlrLineAttributes openlrLineAttributes, int level) {
		System.out.println(indent(level) + "OpenlrLineAttributes:");
		if (openlrLineAttributes.getOpenlrFunctionalRoadClass() != null)
			System.out.println(indent(level + 1) + "OpenlrFunctionalRoadClass: " + openlrLineAttributes.getOpenlrFunctionalRoadClass().name());
		if (openlrLineAttributes.getOpenlrFormOfWay() != null)
			System.out.println(indent(level + 1) + "OpenlrFormOfWay: " + openlrLineAttributes.getOpenlrFormOfWay().name());
		if (openlrLineAttributes.getOpenlrBearing() != null)
			System.out.println(indent(level + 1) + "OpenlrBearing: " + openlrLineAttributes.getOpenlrBearing());
	}

	private static void dissectOpenlrLineLocationReference(OpenlrLineLocationReference openlrLineLocationReference,
			String name, int level) {
		System.out.println(indent(level) + name + ":");
		if (openlrLineLocationReference.getOpenlrLocationReferencePoint() != null) {
			System.out.println(indent(level + 1) + "OpenlrLocationReferencePoint: ("
					+ openlrLineLocationReference.getOpenlrLocationReferencePoint().size() + ")");
			for (OpenlrLocationReferencePoint point : openlrLineLocationReference.getOpenlrLocationReferencePoint())
				dissectOpenlrBaseLocationReferencePoint(point, point.getClass().getSimpleName(), level + 2);
		}
		if (openlrLineLocationReference.getOpenlrLastLocationReferencePoint() != null)
			dissectOpenlrBaseLocationReferencePoint(openlrLineLocationReference.getOpenlrLastLocationReferencePoint(),
					"OpenlrLastLocationReferencePoint", level + 1);
		if (openlrLineLocationReference.getOpenlrOffsets() != null)
			dissectOpenlrOffsets(openlrLineLocationReference.getOpenlrOffsets(), level + 1);
	}

	private static void dissectOpenlrLocationReferencePoint(
			OpenlrLocationReferencePoint openlrBaseLocationReferencePoint, int level) {
		if (openlrBaseLocationReferencePoint.getOpenlrPathAttributes() != null)
			dissectOpenlrPathAttributes(openlrBaseLocationReferencePoint.getOpenlrPathAttributes(), level);
	}

	private static void dissectOpenlrOffsets(OpenlrOffsets openlrOffsets, int level) {
		System.out.println(indent(level) + "OpenlrOffsets:");
		if (openlrOffsets.getOpenlrPositiveOffset() != null)
			System.out.println(indent(level + 1) + "PositiveOffset: " + openlrOffsets.getOpenlrPositiveOffset());
		if (openlrOffsets.getOpenlrNegativeOffset() != null)
			System.out.println(indent(level + 1) + "NegativeOffset: " + openlrOffsets.getOpenlrNegativeOffset());
	}

	private static void dissectOpenlrPathAttributes(OpenlrPathAttributes openlrPathAttributes, int level) {
		System.out.println(indent(level) + "OpenlrPathAttributes:");
		if (openlrPathAttributes.getOpenlrLowestFRCToNextLRPoint() != null)
			System.out.println(indent(level + 1) + "OpenlrLowestFRCToNextLRPoint: " + openlrPathAttributes.getOpenlrLowestFRCToNextLRPoint().name());
		if (openlrPathAttributes.getOpenlrDistanceToNextLRPoint() != null)
			System.out.println(indent(level + 1) + "OpenlrDistanceToNextLRPoint: " + openlrPathAttributes.getOpenlrDistanceToNextLRPoint());
	}

	private static void dissectOverallPeriod(OverallPeriod overallPeriod, int level) {
		System.out.println(indent(level) + "ValidityTimeSpecification: OverallPeriod");
		if (overallPeriod.getOverallStartTime() != null)
			System.out.println(indent(level + 1) + "OverallStartTime: " + overallPeriod.getOverallStartTime());
		if (overallPeriod.getOverallEndTime() != null)
			System.out.println(indent(level + 1) + "OverallEndTime: " + overallPeriod.getOverallEndTime());
		if (overallPeriod.getValidPeriod() != null)
			dissectPeriod(overallPeriod.getValidPeriod(), "ValidPeriod", level + 1);
		if (overallPeriod.getExceptionPeriod() != null)
			dissectPeriod(overallPeriod.getExceptionPeriod(), "ExceptionPeriod", level + 1);
	}

	private static void dissectPayloadPublication(PayloadPublication payloadPublication, int level) {
		System.out.println(indent(level) + "PayloadPublication: " + payloadPublication.getClass().getSimpleName());
		System.out.println(indent(level + 1) + "FeedType: " + payloadPublication.getFeedType());
		System.out.println(indent(level + 1) + "PublicationTime: " + payloadPublication.getPublicationTime());
		InternationalIdentifier iid = payloadPublication.getPublicationCreator();
		if (iid != null) {
			System.out.println(indent(level + 1) + "Publication Creator:");
			System.out.println(indent(level + 2) + "Country: " + iid.getCountry().name());
			System.out.println(indent(level + 2) + "National Identifier: " + iid.getNationalIdentifier());
		} else
			System.out.println(indent(level + 1) + "Publication Creator: null");
		if (payloadPublication instanceof SituationPublication) {
			dissectSituationPublication((SituationPublication) payloadPublication, level + 1);
		}
		// TODO other payloadPublication classes
	}

	private static void dissectPercentageDistanceAlongLinearElement(
			PercentageDistanceAlongLinearElement percentageDistanceAlongLinearElement, int level) {
		System.out.println(indent(level) + "PercentageDistanceAlong: " + percentageDistanceAlongLinearElement.getPercentageDistanceAlong());
	}

	private static void dissectPeriod(List<Period> periods, String name, int level) {
		System.out.println(indent(level) + name + ": (" + periods.size() + ")");
		for (Period period : periods)
			dissectPeriod(period, level + 1);
	}

	private static void dissectPeriod(Period period, int level) {
		System.out.println(indent(level) + "Period:");
		if (period.getStartOfPeriod() != null)
			System.out.println(indent(level + 1) + "StartOfPeriod: " + period.getStartOfPeriod());
		if (period.getEndOfPeriod() != null)
			System.out.println(indent(level + 1) + "EndOfPeriod: " + period.getEndOfPeriod());
		if (period.getPeriodName() != null)
			dissectMultilingualString(period.getPeriodName(), "PeriodName", level + 1);
		if (period.getRecurringTimePeriodOfDay() != null)
			dissectTimePeriodOfDay(period.getRecurringTimePeriodOfDay(), "RecurringTimePeriodOfDay", level + 1);
		if (period.getRecurringDayWeekMonthPeriod() != null)
			dissectDayWeekMonth(period.getRecurringDayWeekMonthPeriod(), "RecurringDayWeekMonthPeriod", level + 1);
	}

	private static void dissectPoint(Point point, int level) {
		if (point.getTpegPointLocation() != null)
			dissectTpegPointLocation(point.getTpegPointLocation(), level);
		if (point.getAlertCPoint() != null)
			dissectAlertCPoint(point.getAlertCPoint(), level);
		if (point.getPointAlongLinearElement() != null)
			dissectPointAlongLinearElement(point.getPointAlongLinearElement(), level);
		if (point.getPointByCoordinates() != null)
			dissectPointByCoordinates(point.getPointByCoordinates(), level);
	}

	private static void dissectPointAlongLinearElement(PointAlongLinearElement pointAlongLinearElement, int level) {
		System.out.println(indent(level) + "PointAlongLinearElement:");
		if (pointAlongLinearElement.getAdministrativeAreaOfPoint() != null)
			dissectMultilingualString(pointAlongLinearElement.getAdministrativeAreaOfPoint(), "AdministrativeAreaOfPoint", level + 1);
		if (pointAlongLinearElement.getDirectionBoundAtPoint() != null)
			System.out.println(indent(level + 1) + "DirectionBoundAtPoint: " + pointAlongLinearElement.getDirectionBoundAtPoint().name());
		if (pointAlongLinearElement.getDirectionRelativeAtPoint() != null)
			System.out.println(indent(level + 1) + "DirectionRelativeAtPoint: " + pointAlongLinearElement.getDirectionRelativeAtPoint().name());
		if (pointAlongLinearElement.getHeightGradeOfPoint() != null)
			System.out.println(indent(level + 1) + "HeightGradeOfPoint: " + pointAlongLinearElement.getHeightGradeOfPoint().name());
		if (pointAlongLinearElement.getLinearElement() != null)
			dissectLinearElement(pointAlongLinearElement.getLinearElement(), level + 1);
		if (pointAlongLinearElement.getDistanceAlongLinearElement() != null)
			dissectDistanceAlongLinearElement(pointAlongLinearElement.getDistanceAlongLinearElement(), "DistanceAlongLinearElement", level + 1);
	}

	private static void dissectPointByCoordinates(PointByCoordinates pointByCoordinates, int level) {
		System.out.println(indent(level) + "PointByCoordinates:");
		System.out.println(indent(level + 1) + "Bearing: " + pointByCoordinates.getBearing());
		if (pointByCoordinates.getPointCoordinates() != null)
			dissectPointCoordinates(pointByCoordinates.getPointCoordinates(), "PointCoordinates", level + 1);
	}

	private static void dissectPointCoordinates(PointCoordinates pointCoordinates, String name, int level) {
		System.out.println(indent(level) + name + ": " + pointCoordinates.getLatitude() + " " + pointCoordinates.getLongitude());
		dissectLatLon(pointCoordinates.getLatitude(), pointCoordinates.getLongitude(), level + 1);
	}

	private static void dissectPointDestination(PointDestination pointDestination, int level) {
		if (pointDestination.getPoint() != null) {
			System.out.println(indent(level) + "Point:");
			dissectPoint(pointDestination.getPoint(), level + 1);
		}
	}

	private static void dissectPoorEnvironmentConditions(PoorEnvironmentConditions poorEnvironmentConditions, int level) {
		if (poorEnvironmentConditions.getPoorEnvironmentType() != null) {
			System.out.println(indent(level) + "PoorEnvironmentType:");
			for (PoorEnvironmentTypeEnum type : poorEnvironmentConditions.getPoorEnvironmentType())
				System.out.println(indent(level + 1) + type.name());
		}
		// TODO PrecipitationDetail precipitationDetail
		// TODO Visibility visibility
		// TODO Pollution pollution
		// TODO Temperature temperature
		// TODO Wind wind
		// TODO Humidity humidity
	}

	private static void dissectPublicEvent(PublicEvent publicEvent, int level) {
		if (publicEvent.getPublicEventType() != null)
			System.out.println(indent(level) + "EventType: " + publicEvent.getPublicEventType().name());
	}

	private static void dissectReferent(Referent referent, String name, int level) {
		System.out.println(indent(level) + name + ":");
		System.out.println(indent(level + 1) + "ReferentIdentifier: " + referent.getReferentIdentifier());
		System.out.println(indent(level + 1) + "ReferentName: " + referent.getReferentName());
		if (referent.getReferentType() != null)
			System.out.println(indent(level + 1) + "ReferentType: " + referent.getReferentType().name());
		if (referent.getReferentDescription() != null)
			dissectMultilingualString(referent.getReferentDescription(), "ReferentDescription", level + 1);
		if (referent.getPointCoordinates() != null)
			dissectPointCoordinates(referent.getPointCoordinates(), "PointCoordinates", level + 1);
	}

	private static void dissectReroutingManagement (ReroutingManagement reroutingManagement, int level) {
		if (reroutingManagement.getReroutingManagementType() != null) {
			System.out.println(indent(level) + "ReroutingManagementType:");
			for (ReroutingManagementTypeEnum type : reroutingManagement.getReroutingManagementType())
				System.out.println(indent(level + 1) + type.name());
		}
		// skip reroutingItineraryDescription (prose text)
		if (reroutingManagement.isSignedRerouting() != null)
			System.out.println(indent(level) + "SignedRerouting: " + (reroutingManagement.isSignedRerouting() ? "true" : "false"));
		System.out.println(indent(level) + "Entry: " + reroutingManagement.getEntry());
		System.out.println(indent(level) + "Exit: " + reroutingManagement.getExit());
		System.out.println(indent(level) + "RoadOrJunctionNumber: " + reroutingManagement.getRoadOrJunctionNumber());
		if (reroutingManagement.getAlternativeRoute() != null)
			System.out.println(indent(level) + "AlternativeRoute: (" + reroutingManagement.getAlternativeRoute().size() + ")");
	}

	private static void dissectRoad(Road road, int level) {
		System.out.println(indent(level) + "RoadNumber: " + road.roadNumber);
	}

	private static void dissectRoadConditions(RoadConditions roadConditions, int level) {
		// nothing to do here, apart from extensions
		if (roadConditions instanceof WeatherRelatedRoadConditions) {
			dissectWeatherRelatedRoadConditions((WeatherRelatedRoadConditions) roadConditions, level);
		} else if (roadConditions instanceof NonWeatherRelatedRoadConditions) {
			System.out.println(indent(level) + "NonWeatherRelatedRoadConditions (TODO)");
			// TODO
		}
	}

	private static void dissectRoadOrCarriagewayOrLaneManagement(RoadOrCarriagewayOrLaneManagement roadOrCarriagewayOrLaneManagement, int level) {
		if (roadOrCarriagewayOrLaneManagement.getRoadOrCarriagewayOrLaneManagementType() != null)
			System.out.println(indent(level) + "RoadOrCarriagewayOrLaneManagementType: " + roadOrCarriagewayOrLaneManagement.getRoadOrCarriagewayOrLaneManagementType().name());
		System.out.println(indent(level) + "MinimumCarOccupancy: " + roadOrCarriagewayOrLaneManagement.getMinimumCarOccupancy());
	}

	private static void dissectRoadworks(Roadworks roadworks, int level) {
		if (roadworks.getRoadworksDuration() != null)
			System.out.println(indent(level) + "RoadworksDuration: " + roadworks.getRoadworksDuration().name());
		if (roadworks.getRoadworksScale() != null)
			System.out.println(indent(level) + "RoadworksScale: " + roadworks.getRoadworksScale().name());
		if (roadworks.isUnderTraffic() != null)
			System.out.println(indent(level) + "UnderTraffic: " + (roadworks.isUnderTraffic() ? "true" : "false"));
		if (roadworks.isUrgentRoadworks() != null)
			System.out.println(indent(level) + "UrgentRoadworks: " + (roadworks.isUrgentRoadworks() ? "true" : "false"));
		if ((roadworks.getMobility() != null) && (roadworks.getMobility().getMobilityType() != null))
			System.out.println(indent(level) + "Mobility: " + roadworks.getMobility().getMobilityType().name());
		// TODO subjects
		// TODO maintenanceVehicles
		if (roadworks instanceof ConstructionWorks) {
			dissectConstructionWorks((ConstructionWorks) roadworks, level);
		} else if (roadworks instanceof MaintenanceWorks) {
			dissectMaintenanceWorks((MaintenanceWorks) roadworks, level);
		}
	}

	private static void dissectSituation(Situation situation, int level) {
		System.out.println(indent(level) + "Situation: " + situation.getId());
		System.out.println(indent(level + 1) + "SituationVersionTime: " + situation.getSituationVersionTime());
		System.out.println(indent(level + 1) + "SituationRecords: " + situation.getSituationRecord().size());
		for (SituationRecord situationRecord : situation.getSituationRecord())
			dissectSituationRecord(situationRecord, level + 2);
	}

	private static void dissectSituationPublication(SituationPublication situationPublication, int level) {
		List<Situation> situations = situationPublication.getSituation();
		System.out.println(indent(level) + "Situations: " + situations.size());
		for (Situation situation : situations)
			dissectSituation(situation, level + 1);
	}

	private static void dissectSituationRecord(SituationRecord situationRecord, int level) {
		System.out.println(indent(level) + "SituationRecord: " + situationRecord.getClass().getSimpleName());
		System.out.println(indent(level + 1) + "Id: " + situationRecord.getId());
		System.out.println(indent(level + 1) + "CreationTime: " + situationRecord.getSituationRecordCreationTime());
		System.out.println(indent(level + 1) + "ObservationTime: " + situationRecord.getSituationRecordObservationTime());
		System.out.println(indent(level + 1) + "VersionTime: " + situationRecord.getSituationRecordVersionTime());
		System.out.println(indent(level + 1) + "FirstSupplierVersionTime: " + situationRecord.getSituationRecordFirstSupplierVersionTime());
		if (situationRecord.getValidity() != null)
			dissectValidity(situationRecord.getValidity(), level + 1);
		dissectGroupOfLocations(situationRecord.getGroupOfLocations(), level + 1);
		System.out.println(indent(level + 1) + "Situation data:");
		if (situationRecord.getCause() != null)
			dissectCause(situationRecord.getCause(), level + 2);
		if (situationRecord.getImpact() != null)
			dissectImpact(situationRecord.getImpact(), level + 2);
		if (situationRecord instanceof GenericSituationRecord) {
			System.out.println(indent(level + 2) + "GenericSituationRecord (TODO)");
			// TODO
		} else if (situationRecord instanceof NonRoadEventInformation) {
			dissectNonRoadEventInformation((NonRoadEventInformation) situationRecord, level + 2);
		} else if (situationRecord instanceof OperatorAction) {
			dissectOperatorAction((OperatorAction) situationRecord, level + 2);
		} else if (situationRecord instanceof TrafficElement) {
			dissectTrafficElement((TrafficElement) situationRecord, level + 2);
		}
	}

	private static void dissectSupplementaryPositionalDescription(
			SupplementaryPositionalDescription supplementaryPositionalDescription, int level) {
		System.out.println(indent(level) + "SupplementaryPositionalDescription:");
		if (supplementaryPositionalDescription.getLocationDescriptor() != null) {
			System.out.println(indent(level + 1) + "LocationDescriptor:");
			for (LocationDescriptorEnum descriptor : supplementaryPositionalDescription.getLocationDescriptor())
				System.out.println(indent(level + 2) + descriptor.name());
		}
		if (supplementaryPositionalDescription.getSequentialRampNumber() != null)
			System.out.println(indent(level + 1) + "SequentialRampNumber: " + supplementaryPositionalDescription.getSequentialRampNumber());
		// TODO List<AffectedCarriagewayAndLanes> affectedCarriagewayAndLanes
		if (supplementaryPositionalDescription.getLocationPrecision() != null)
			System.out.println(indent(level + 1) + "LocationPrecision: " + supplementaryPositionalDescription.getLocationPrecision());
	}

	private static void dissectSpeedManagement (SpeedManagement speedManagement, int level) {
		if (speedManagement.getSpeedManagementType() != null)
			System.out.println(indent(level) + "SpeedManagementType: " + speedManagement.getSpeedManagementType().name());
		System.out.println(indent(level) + "temporarySpeedLimit: " + speedManagement.getTemporarySpeedLimit());
	}

	private static void dissectTimePeriodByHour(TimePeriodByHour timePeriodByHour, int level) {
		if (timePeriodByHour.getStartTimeOfPeriod() != null)
			System.out.println(indent(level) + "StartTimeOfPeriod: " + timePeriodByHour.getStartTimeOfPeriod().toString());
		if (timePeriodByHour.getEndTimeOfPeriod() != null)
			System.out.println(indent(level) + "EndTimeOfPeriod: " + timePeriodByHour.getEndTimeOfPeriod().toString());
	}

	private static void dissectTimePeriodOfDay(List<TimePeriodOfDay> recurringTimePeriodOfDay, String name, int level) {
		System.out.println(indent(level) + name + ": (" + recurringTimePeriodOfDay.size() + ")");
		for (TimePeriodOfDay timePeriodOfDay : recurringTimePeriodOfDay)
			dissectTimePeriodOfDay(timePeriodOfDay, level + 1);
	}

	private static void dissectTimePeriodOfDay(TimePeriodOfDay timePeriodOfDay, int level) {
		System.out.println(indent(level) + "TimePeriodOfDay: " + timePeriodOfDay.getClass().getSimpleName());
		if (timePeriodOfDay instanceof TimePeriodByHour)
			dissectTimePeriodByHour((TimePeriodByHour) timePeriodOfDay, level + 1);
	}

	private static void dissectTpegAreaDescriptor(TpegAreaDescriptor tpegAreaDescriptor, int level) {
		System.out.println(indent(level) + "TpegAreaDescriptor (TODO)");
		// TODO
	}

	private static void dissectTpegAreaLocation(TpegAreaLocation tpegAreaLocation, int level) {
		System.out.println(indent(level) + "TpegAreaLocation (TODO)");
		// TODO
	}

	private static void dissectTpegDescriptor(TpegDescriptor tpegDescriptor, int level) {
		if (tpegDescriptor.getDescriptor() != null)
			dissectMultilingualString(tpegDescriptor.getDescriptor(), "Descriptor", level);
		if (tpegDescriptor instanceof TpegAreaDescriptor) {
			dissectTpegAreaDescriptor((TpegAreaDescriptor) tpegDescriptor, level);
		} else if (tpegDescriptor instanceof TpegPointDescriptor) {
			dissectTpegPointDescriptor((TpegPointDescriptor) tpegDescriptor, level);
		}
	}

	private static void dissectTpegIlcPointDescriptor(TpegIlcPointDescriptor tpegIlcPointDescriptor, int level) {
		System.out.println(indent(level) + "TpegIlcPointDescriptor (TODO)");
		// TODO
	}

	private static void dissectTpegJunction(TpegJunction tpegPoint, int level) {
		System.out.println(indent(level) + "TpegJunction (TODO)");
		// TODO
	}

	private static void dissectTpegJunctionPointDescriptor(TpegJunctionPointDescriptor tpegJunctionPointDescriptor, int level) {
		System.out.println(indent(level) + "TpegJunctionPointDescriptor (TODO)");
		// TODO
	}

	private static void dissectTpegLinearLocation(TpegLinearLocation tpegLinearLocation, int level) {
		System.out.println(indent(level) + "TpegLinearLocation:");
		if (tpegLinearLocation.getTpegDirection() != null)
			System.out.println(indent(level + 1) + "TpegDirection: " + tpegLinearLocation.getTpegDirection());
		if (tpegLinearLocation.getTpegLinearLocationType() != null)
			System.out.println(indent(level + 1) + "TpegLinearLocationType: " + tpegLinearLocation.getTpegLinearLocationType());
		if (tpegLinearLocation.getFrom() != null) {
			System.out.println(indent(level + 1) + "From: " + tpegLinearLocation.getFrom().getClass().getSimpleName());
			dissectTpegPoint(tpegLinearLocation.getFrom(), level + 2);
		}
		if (tpegLinearLocation.getTo() != null) {
			System.out.println(indent(level) + "To: " + tpegLinearLocation.getTo().getClass().getSimpleName());
			dissectTpegPoint(tpegLinearLocation.getTo(), level + 2);
		}
	}

	private static void dissectTpegNonJunctionPoint(TpegNonJunctionPoint tpegPoint, int level) {
		if (tpegPoint.getPointCoordinates() != null)
			dissectPointCoordinates(tpegPoint.getPointCoordinates(), "PointCoordinates", level);
		if (tpegPoint.getName() != null) {
			System.out.println(indent(level) + "Name: ");
			for (TpegOtherPointDescriptor descriptor : tpegPoint.getName())
				dissectTpegDescriptor(descriptor, level + 1);
		}
	}

	private static void dissectTpegOtherPointDescriptor(TpegOtherPointDescriptor tpegOtherPointDescriptor, int level) {
		if (tpegOtherPointDescriptor.getTpegOtherPointDescriptorType() != null)
			System.out.println(indent(level) + "TpegOtherPointDescriptorType: " + tpegOtherPointDescriptor.getTpegOtherPointDescriptorType().name());
	}

	private static void dissectTpegPoint(TpegPoint tpegPoint, int level) {
		// nothing to do here, apart from extensions
		if (tpegPoint instanceof TpegJunction) {
			dissectTpegJunction((TpegJunction) tpegPoint, level);
		} else if (tpegPoint instanceof TpegNonJunctionPoint) {
			dissectTpegNonJunctionPoint((TpegNonJunctionPoint) tpegPoint, level);
		}
	}

	private static void dissectTpegPointDescriptor(TpegPointDescriptor tpegPointDescriptor, int level) {
		// nothing to do here, apart from extensions
		if (tpegPointDescriptor instanceof TpegIlcPointDescriptor) {
			dissectTpegIlcPointDescriptor((TpegIlcPointDescriptor) tpegPointDescriptor, level);
		} else if (tpegPointDescriptor instanceof TpegJunctionPointDescriptor) {
			dissectTpegJunctionPointDescriptor((TpegJunctionPointDescriptor) tpegPointDescriptor, level);
		} else if (tpegPointDescriptor instanceof TpegOtherPointDescriptor) {
			dissectTpegOtherPointDescriptor((TpegOtherPointDescriptor) tpegPointDescriptor, level);
		}
	}

	private static void dissectTpegPointLocation(TpegPointLocation tpegPointLocation, int level) {
		System.out.println(indent(level) + "TpegPointLocation (TODO)");
		// TODO
	}

	private static void dissectTrafficElement(TrafficElement trafficElement, int level) {
		// nothing to do here, apart from extensions
		if (trafficElement instanceof AbnormalTraffic) {
			dissectAbnormalTraffic((AbnormalTraffic) trafficElement, level);
		} else if (trafficElement instanceof Accident) {
			dissectAccident((Accident) trafficElement, level);
		} else if (trafficElement instanceof Activity) {
			dissectActivity((Activity) trafficElement, level);
		} else if (trafficElement instanceof Conditions) {
			dissectConditions((Conditions) trafficElement, level);
		} else if (trafficElement instanceof EquipmentOrSystemFault) {
			System.out.println(indent(level) + "EquipmentOrSystemFault (TODO)");
			// TODO
		} else if (trafficElement instanceof Obstruction) {
			dissectObstruction((Obstruction) trafficElement, level);
		}
	}

	private static void dissectValidity(Validity validity, int level) {
		System.out.println(indent(level) + "Validity:");
		if (validity.getValidityStatus() != null)
			System.out.println(indent(level + 1) + "ValidityStatus: " + validity.getValidityStatus().name());
		if (validity.isOverrunning() != null)
			System.out.println(indent(level + 1) + "Overrunning: " + (validity.isOverrunning() ? "true" : "false"));
		if (validity.getValidityTimeSpecification() != null)
			dissectOverallPeriod(validity.getValidityTimeSpecification(), level + 1);
	}

	private static void dissectVehicleObstruction(VehicleObstruction vehicleObstruction, int level) {
		if (vehicleObstruction.getVehicleObstructionType() != null)
			System.out.println(indent(level) + "VehicleObstructionType: " + vehicleObstruction.getVehicleObstructionType().name());
	}

	private static void dissectWeatherRelatedRoadConditions(WeatherRelatedRoadConditions weatherRelatedRoadConditions, int level) {
		if (weatherRelatedRoadConditions.getWeatherRelatedRoadConditionType() != null) {
			System.out.println(indent(level) + "WeatherRelatedRoadConditionType:");
			for (WeatherRelatedRoadConditionTypeEnum type : weatherRelatedRoadConditions.getWeatherRelatedRoadConditionType())
				System.out.println(indent(level + 1) + type.name());
		}
	}

	/**
	 * @brief Returns a string with the appropriate number of spaces for the requested indentation level
	 * 
	 * @param level
	 */
	private static String indent(int level) {
		String res = "";
		for (int i = 0; i < level; i++)
			res += "  ";
		return res;
	}

}
