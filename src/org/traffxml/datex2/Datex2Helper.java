package org.traffxml.datex2;
/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml-datex2 library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.TraffSupplementaryInfo;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.datex2.schema._2._2_0.D2LogicalModel;
import eu.datex2.schema._2._2_0.Situation;
import eu.datex2.schema._2._2_0.SituationPublication;
import eu.datex2.schema._2._2_0.SituationRecord;
import eu.datex2.schema._2._2_0.Validity;
import eu.datex2.schema._2._2_0.ValidityStatusEnum;

/**
 * Helper class for Datex-2 to TraFF conversion.
 * 
 * <p>This class exposes all the methods needed to retrieve a Datex-2 feed and convert it to TraFF.
 */
public class Datex2Helper {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	/** Pattern for parsing data files */
	static final Pattern COLON_PATTERN = Pattern.compile(";");

	static JAXBContext jc = null;

	/**
	 * Returns the AlertC country identifier (CID) for a given ISO country code.
	 * 
	 * @param iso The ISO country code, case-insensitive
	 * 
	 * @return The AlertC country code, or null if unknown
	 */
	public static Integer getCidFromIso(String iso) {
		return Datex2LocationHelper.ISO_TO_CID.get(iso.toUpperCase());
	}

	/**
	 * Retrieves a D2LogicalModel from an XML input stream.
	 * 
	 * @param inputStream The XML input stream
	 * @return The D2LogicalModel, or null if an error occurred
	 */
	@SuppressWarnings("unchecked")
	public static D2LogicalModel getD2LogicalModel(InputStream inputStream) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(inputStream);
			Node rootNode = doc.getDocumentElement();
			/*
			 * Find the root of the D2LogicalModel (not necessarily the root node of the document, may be
			 * wrapped in a SOAP envelope or similar)
			 */
			NodeList nl = doc.getElementsByTagNameNS("http://datex2.eu/schema/2/2_0", "d2LogicalModel");
			if (nl.getLength() > 0)
				rootNode = nl.item(0);
			else {
				/*
				 * If no compliant D2LogicalModel was found, produce debug output.
				 * This is the case with Level C, or schema major versions other than 2, both of which are noncompliant
				 * with the Datex 2 version 2 schema.
				 */
				LOG.warn("Input was not recognized as valid Datex 2");
				LOG.debug("Document element: " + doc.getDocumentElement().getLocalName() + " (" + doc.getDocumentElement().getNamespaceURI() + ")");
				LOG.debug("  FirstChild: " + doc.getDocumentElement().getFirstChild().getLocalName() + " (" + doc.getDocumentElement().getFirstChild().getNamespaceURI() + ")");
				nl = doc.getElementsByTagName("d2LogicalModel");
				LOG.debug("  ElementsByTagName(\"d2LogicalModel\"): " + nl.getLength());
				for (int i = 0; i < nl.getLength(); i++)
					LOG.debug("    " + nl.item(i).getLocalName() + " (" + nl.item(i).getNamespaceURI() + ")");
				/*
				 * If a D2LogicalModel node was found, try to unmarshal it so we can examine the exception
				 */
				if (nl.getLength() > 0)
					rootNode = nl.item(0);
			}

			JAXBElement<D2LogicalModel> root;
			if (jc == null)
				jc = JAXBContext.newInstance(D2LogicalModel.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			root = (JAXBElement<D2LogicalModel>)unmarshaller.unmarshal(rootNode);
			D2LogicalModel d2lm = root.getValue();
			return d2lm;
		} catch (JAXBException e) {
			LOG.debug("{}", e);
			return null;
		} catch (ParserConfigurationException e) {
			LOG.debug("{}", e);
			return null;
		} catch (SAXException e) {
			LOG.debug("{}", e);
			return null;
		} catch (IOException e) {
			LOG.debug("{}", e);
			return null;
		}
	}

	/**
	 * Converts a Datex 2 feed to a list of TraFF messages.
	 * 
	 * <p>This method allows previously received messages to be specified as an optional argument, in order for
	 * information from those messages to be used in the feed.
	 * 
	 * <p>{@code forceCc} and {@code forceLtn} are used for Alert-C resolution. If non-null, the CC and LTN
	 * in the Datex 2 feed are ignored and the supplied values are used; this is necessary with many feeds
	 * which contain invalid values. If they are null, the respective value is taken from the feed. If the
	 * feed does not use Alert-C location references, these arguments take no effect.
	 * 
	 * @param d2lm The Datex 2 feed
	 * @param sourceIdPrefix The prefix for the TraFF source ID
	 * @param forceCc An ISO country code (two letters) or RDS country code (one hex digit)
	 * @param forceLtn A location table number
	 * @param pollInterval Interval at which this source is being polled, in minutes
	 * @param oldMessages Previously received messages
	 */
	public static List<TraffMessage> toTraff(D2LogicalModel d2lm, String sourceIdPrefix,
			String forceCc, String forceLtn,
			int pollInterval, Collection<TraffMessage> oldMessages) {
		D2Context d2Context = new D2Context(d2lm, sourceIdPrefix, forceCc, forceLtn);
		Map<String, TraffMessage> outMap = new HashMap<String, TraffMessage>();
		if (d2lm.getPayloadPublication() instanceof SituationPublication) {
			List<Situation> situations = ((SituationPublication) d2lm.getPayloadPublication()).getSituation();
			for (Situation situation : situations)
				for (TraffMessage message : toTraff(d2Context, situation, sourceIdPrefix, pollInterval))
					outMap.put(message.id, message);
			/* Add cancellations for all unmatched old messages */
			if (oldMessages != null)
				for (TraffMessage oldMessage : oldMessages)
					if (oldMessage.id.startsWith(sourceIdPrefix + ":")
							&& !outMap.containsKey(oldMessage.id)) {
						TraffMessage.Builder builder = new TraffMessage.Builder();
						builder.setId(oldMessage.id);
						builder.setReceiveTime(oldMessage.receiveTime);
						builder.setUpdateTime(new Date());
						Date expirationTime = oldMessage.expirationTime;
						if (expirationTime == null)
							expirationTime = oldMessage.endTime;
						builder.setExpirationTime(expirationTime);
						builder.setCancellation(true);
						outMap.put(oldMessage.id, builder.build());
					}
		} else
			throw new IllegalArgumentException("payloadPublication type "
					+ d2lm.getPayloadPublication().getClass().getSimpleName() + " is not supported");
		return new ArrayList<TraffMessage>(outMap.values());
	}

	/**
	 * Converts a Datex 2 situation to a list of TraFF messages.
	 * 
	 * <p>If all situation records of the situation are mergeable (see below), the situation record is
	 * translated into a single message, and the ID of the message is the ID of the situation,
	 * prefixed with the source ID and {@code :s:}.
	 * 
	 * <p>In order to be mergeable, all situation records must share the same location. This is
	 * currently determined by determining the TraFF location for each situation record and
	 * comparing them using {@link TraffLocation#DEFAULT_MATCHER}. Future implementations may
	 * change matching criteria by allowing some variation in the representation, as long as all
	 * locations refer to the same location on the ground.
	 * 
	 * <p>Different start and end times (if set) also preclude mergeability. Currently, if only some
	 * situation records specify a start and/or end time while others do not, they are still
	 * merged, and the start/end times are applied to all. This behavior may change in future
	 * implementations. In any case, if two or more situation records specify different a start
	 * and/or end times, they cannot be merged.
	 * 
	 * <p>If merging is not possible, each situation record is translated into a separate message and
	 * the ID of the message is the ID of the situation record, prefixed with the source ID and
	 * {@code :r:}.
	 * 
	 * <p>Partial merge operations are currently not performed, mainly due to lack of an ID candidate
	 * for messages resulting from partial merges.
	 * 
	 * <p>If a situation record translates to supplementary information items without any messages,
	 * the SI items are attached to another message with a matching location. This is done even
	 * when merging is not possible; the ID of the situation record which generated the SI items is
	 * not preserved in this case. If no message with a matching location exists, the SI items are
	 * discarded and a warning is logged.
	 * 
	 * @param context The D2Context, can be null
	 * @param situation The situation to convert
	 * @param sourceIdPrefix The prefix for the TraFF source ID
	 * @param pollInterval Interval at which this source is being polled, in minutes
	 */
	public static Collection<TraffMessage> toTraff(D2Context context, Situation situation, String sourceIdPrefix,
			int pollInterval) {
		SituationContext childContext = new SituationContext(context, situation);
		List<TraffMessage> res = new ArrayList<TraffMessage>();
		boolean siDiscarded = false;
		for (SituationRecord situationRecord : situation.getSituationRecord())
			try {
				TraffMessage message = toTraff(childContext, situationRecord, sourceIdPrefix, pollInterval);
				if (message != null)
					res.add(message);
			} catch (Exception e) {
				LOG.warn("Skipping SituationRecord " + situationRecord.getId());
				LOG.debug("{}", e);
			}

		/* merge SituationRecords of the same Situation if locations match */
		if (res.size() > 1) {
			TraffLocation location = null;
			boolean canMerge = true;
			Date receiveTime = null;
			Date updateTime = null;
			Date expirationTime = null;
			Date startTime = null;
			Date endTime = null;

			TraffMessage.Builder builder = new TraffMessage.Builder();
			builder.setId(sourceIdPrefix + ":s:" + situation.getId());

			for (TraffMessage message : res) {
				if (location == null)
					location = message.location;
				else if (!TraffLocation.DEFAULT_MATCHER.match(location, message.location)) {
					canMerge = false;
					break;
				}
				builder.addEvents(message.getEventsAsList());
				if ((receiveTime == null) || ((message.receiveTime != null) && (message.receiveTime.before(receiveTime))))
					receiveTime = message.receiveTime;
				if ((updateTime == null) || ((message.updateTime != null) && (message.updateTime.after(updateTime))))
					updateTime = message.updateTime;
				if ((expirationTime == null) || ((message.expirationTime != null) && (message.expirationTime.after(expirationTime))))
					expirationTime = message.expirationTime;
				if (startTime == null)
					startTime = message.startTime;
				else if ((message.startTime != null) && (startTime.compareTo(message.startTime) != 0)) {
					canMerge = false;
					break;
				}
				if (endTime == null)
					endTime = message.endTime;
				else if ((message.endTime != null) && (endTime.compareTo(message.endTime) != 0)) {
					canMerge = false;
					break;
				}
			}
			if (canMerge) {
				if ((expirationTime != null) && (startTime != null) && (expirationTime.before(startTime)))
					expirationTime = startTime;
				if ((expirationTime != null) && (endTime != null) && (expirationTime.before(endTime)))
					expirationTime = endTime;
				builder.setReceiveTime(receiveTime);
				builder.setUpdateTime(updateTime);
				builder.setExpirationTime(expirationTime);
				builder.setStartTime(startTime);
				builder.setEndTime(endTime);
				builder.setLocation(location);
				res.clear();
				res.add(builder.build());
			}
		}

		/* attach orphaned SIs to messages with matching locations */
		for (TraffLocation siLocation : childContext.sis.keySet()) {
			TraffMessage matched = null;
			for (TraffMessage message : res)
				if (TraffLocation.DEFAULT_MATCHER.match(message.location, siLocation))
					matched = message;
			if (matched != null) {
				TraffEvent matchedEvent = null;
				for (TraffEvent event : matched.events)
					matchedEvent = event;
				TraffEvent.Builder evBuilder = new TraffEvent.Builder(matchedEvent);
				evBuilder.addSupplementaryInfos(childContext.sis.get(siLocation));
				TraffMessage.Builder builder = new TraffMessage.Builder(matched);
				builder.clearEvents();
				for (TraffEvent event : matched.events)
					if (event != matchedEvent)
						builder.addEvent(event);
				builder.addEvent(evBuilder.build());
				res.remove(matched);
				res.add(builder.build());
			} else
				siDiscarded = true;
		}
		if (siDiscarded)
			LOG.warn("SIs discarded (no event at matching location) for situation {}",
					situation.getId());
		return res;
	}

	private static void parseValidity(SituationRecordContext context, Validity validity) {
		if (validity.getValidityTimeSpecification() == null)
			return;

		/*
		 * Some sources set start and end times but report validity status as ACTIVE during that time.
		 * In these cases, use end time as expiration time.
		 */
		if (validity.getValidityTimeSpecification().getOverallEndTime() != null)
			context.expirationTime = validity.getValidityTimeSpecification().getOverallEndTime().toGregorianCalendar().getTime();

		// skip the rest if validity status is not defined by time spec
		if (!ValidityStatusEnum.DEFINED_BY_VALIDITY_TIME_SPEC.equals(validity.getValidityStatus()))
			return;
		// FIXME if valid periods are defined, parse those and use the current/next one
		if (validity.getValidityTimeSpecification().getOverallStartTime() != null)
			context.startTime = validity.getValidityTimeSpecification().getOverallStartTime().toGregorianCalendar().getTime();
		context.endTime = context.expirationTime;
	}

	/**
	 * Converts a Datex 2 SituationRecord to a TraFF message.
	 * 
	 * <p>Not every SituationRecord translates to a valid TraFF message, usually because they express a
	 * situation for which there are no corresponding TraFF events. In such cases, null is returned.
	 * 
	 * <p>Expiration time for new messages is calculated in one of the following ways: if {@code situationRecord}
	 * has a validity time specification with a non-null end time, it is used as the expiration time, even if
	 * the validity status indicates that this value should be ignored. If expiration time cannot be derived
	 * in this manner, it is current time plus twice the poll interval plus two minutes. This allows for one
	 * update to be missed without the message expiring.
	 * 
	 * @param context The SituationContext for the enclosing Situation
	 * @param situationRecord The SituationRecord to convert
	 * @param sourceIdPrefix The prefix for the TraFF source ID
	 * @param pollInterval Interval at which this source is being polled, in minutes
	 * 
	 * @return The corresponding TraFF message, or null
	 */
	private static TraffMessage toTraff(SituationContext context, SituationRecord situationRecord, String sourceIdPrefix,
			int pollInterval) {
		SituationRecordContext childContext = new SituationRecordContext(context, situationRecord);
		if (situationRecord.getValidity() != null) {
			if (ValidityStatusEnum.SUSPENDED.equals(situationRecord.getValidity().getValidityStatus()))
				return null;
			parseValidity(childContext, situationRecord.getValidity());
		}

		TraffLocation location = Datex2LocationHelper.getTraffLocationFromGroupOfLocations(childContext, situationRecord.getGroupOfLocations());
		if (location == null)
			return null;

		Collection<TraffEvent> events = Datex2EventHelper.getTraffEventsFromSituationRecord(childContext);
		Collection<TraffSupplementaryInfo> sis = childContext.getSi();
		if (events.isEmpty()) {
			if (!sis.isEmpty())
				/* add “orphaned” SIs to parent context */
				context.addSi(location, sis);
			return null;
		}
		if (!sis.isEmpty()) {
			/* add “orphaned” SIs to last event */
			TraffEvent lastEvent = null;
			for (TraffEvent event : events)
				lastEvent = event;
			TraffEvent.Builder builder = new TraffEvent.Builder(lastEvent);
			for (TraffSupplementaryInfo si : sis)
				builder.addSupplementaryInfo(si);
			events.remove(lastEvent);
			events.add(builder.build());
		}

		TraffMessage.Builder builder = new TraffMessage.Builder();
		builder.setId(sourceIdPrefix + ":r:" + situationRecord.getId());

		Date receiveTime = null;
		Date updateTime = null;
		Date expirationTime = null;
		if (situationRecord.getSituationRecordCreationTime() != null)
			receiveTime = situationRecord.getSituationRecordCreationTime().toGregorianCalendar().getTime();
		if (situationRecord.getSituationRecordVersionTime() != null)
			updateTime = situationRecord.getSituationRecordVersionTime().toGregorianCalendar().getTime();
		if (childContext.expirationTime != null)
			expirationTime = childContext.expirationTime;
		else {
			// validity is twice the poll interval plus two minutes, starting now
			int duration = 2 * pollInterval + 2;
			expirationTime = new Date(Instant.ofEpochMilli((new Date()).getTime()).plusSeconds(duration * 60).toEpochMilli());
		}
		builder.setReceiveTime(receiveTime);
		builder.setUpdateTime(updateTime);
		builder.setExpirationTime(expirationTime);
		builder.setStartTime(childContext.startTime);
		builder.setEndTime(childContext.endTime);

		builder.addEvents(events);

		builder.setLocation(location);

		return builder.build();
	}

	static class D2Context {
		final D2LogicalModel d2LogicalModel;
		final String sourceIdPrefix;
		final String cc;
		final String ltn;
		String country = null;

		D2Context(D2LogicalModel d2LogicalModel, String sourceIdPrefix, String cc, String ltn) {
			super();
			this.d2LogicalModel = d2LogicalModel;
			this.sourceIdPrefix = sourceIdPrefix;
			this.cc = cc;
			this.ltn = ltn;
			try {
				country = d2LogicalModel.getPayloadPublication().getPublicationCreator().getCountry().name();
			} catch (Exception e) {
				// NOP
			}
		}
	}

	static class SituationContext {
		final D2Context parentContext;
		final Situation situation;
		private final Map<TraffLocation, Collection<TraffSupplementaryInfo>> sis = new HashMap<TraffLocation, Collection<TraffSupplementaryInfo>>();

		SituationContext(D2Context parentContext, Situation situation) {
			super();
			this.parentContext = parentContext;
			this.situation = situation;
		}

		void addSi(TraffLocation location, Collection<TraffSupplementaryInfo> sis) {
			// FIXME eliminate duplicates (and merge SIs of equal type, once we start using quantifiers)
			Collection<TraffSupplementaryInfo> oldSis = this.sis.get(location);
			if (oldSis != null)
				oldSis.addAll(sis);
			else
				this.sis.put(location, sis);
		}
	}

	static class SituationRecordContext {
		final SituationContext parentContext;
		final SituationRecord situationRecord;
		Date startTime = null;
		Date endTime = null;
		Date expirationTime = null;
		private final Map<TraffEvent.Type, TraffEvent> events = new HashMap<TraffEvent.Type, TraffEvent>();
		private final Map<TraffSupplementaryInfo.Type, TraffSupplementaryInfo> si = new HashMap<TraffSupplementaryInfo.Type, TraffSupplementaryInfo>();

		SituationRecordContext(SituationContext parentContext, SituationRecord situationRecord) {
			super();
			this.parentContext = parentContext;
			this.situationRecord = situationRecord;
		}

		void addEvent(TraffEvent event) {
			TraffEvent old = events.get(event.type);
			if (old == null)
				events.put(event.type, event);
			else {
				TraffEvent.Builder builder = new TraffEvent.Builder(old);
				builder.merge(event);
				events.put(event.type, builder.build());
			}
		}

		void addSi(TraffSupplementaryInfo si) {
			TraffSupplementaryInfo old = this.si.get(si.type);
			if ((old == null) || (si.quantifier != null))
				this.si.put(si.type, si);
		}

		Collection<TraffEvent> getEvents() {
			return events.values();
		}

		Collection<TraffSupplementaryInfo> getSi() {
			return si.values();
		}
	}
}
